ChangeLog
=========

Unreleased
----------

4.1.0 (2025-01-14)
------------------

* Add client ``ssl_cert`` arg (#57).
* Deprecate ``GrpcClient.call_future`` (#60).
* Update logging of streams (#56, #63).
* Expose ``AsyncGrpcClient`` (#55).
* Add ``Enum`` decoder (#2).
* Update decoder docs (#58).
* Drop ``pytz`` (#59).
* Support async iterators in ``AsyncTestClientMixin`` (#62).
* Fix warning in tests.
* Update project setup.

3.15.2 (2025-01-08)
-------------------

* Forward compatible ``AsyncTestClientMixin.stream_mock`` (#64).

4.0.0 (2024-09-19)
------------------

* Drop support for python 3.7.
* Switch to single service only client (#49).
* Make client arguments keyword only (#43).
* Refactor ``GrpcClient`` to use grpc internal reconnect (#5).
* Drop decoding non-optional empty strings to ``None`` (#24).
* Drop support for ``str`` in ``set_exception_decoder`` (#7).
* Drop blocking argument from ``GrpcClient.call`` (#10).
* Drop ``decode_unset_messages`` and ``shallow_fields`` (#22).
* Unpublish ``frgal.utils.format_message`` (#47).
* Unpublish ``frgal.utils.get_call_id`` (#54).
* Drop automatic ``make_awaitable`` in ``AsyncTestClientMixin`` (#18).

3.15.1 (2024-09-18)
-------------------

* Add support for ``protobuf`` 5 (#52).
* Relax version of ``asyncstdlib`` (#51).
* Handle exceptions without a reason (#50).
* Deprecate ``get_call_id`` from public API (#53).

3.15.0 (2024-08-27)
-------------------

* Rename package to fred-frgal (#4)
* Add single service client (#3)
* Add timeout to clients (#39)
* Add mock_awaits utility (#44)
* Add lazy_format_message for logging (#46)
* Add async semaphore (#40)
* Drop python 3.7, add python 3.12
* Use asyncstdlib instead of aioitertools (#45)
* Fix awrap_stream annotation

3.14.2 (2023-06-08)
-------------------

* Fix async generators in SyncGrpcProxy (#35)
* Add deprecation warning stack level (#38)

3.14.1 (2023-04-03)
-------------------

* Correctly use async service context in service utilities (#37).

3.14.0 (2023-03-27)
-------------------

* Add async service utilities (#34).

3.13.0 (2023-03-01)
-------------------

* Add support for protobuf 4 (#26)
* Remove uses of deprecated asyncio.get_event_loop (#30)
* Remove deprecation warnings about strings being decoded to None in tests (#31)
* Update setup
* Fix typing

3.12.5 (2022-11-08)
-------------------

* Fix annotations

3.12.4 (2022-11-01)
-------------------

* Fix decoding of one field exceptions
* Split message decoding to two private methods – ``_decode_message`` and ``_decode_message_fields``

3.12.3 (2022-09-14)
-------------------

* Fix imports with breaking protobuf 3.20.2

3.12.2 (2022-05-26)
-------------------

* Restrict ``protobuf`` version.

3.12.1 (2022-04-14)
-------------------

* Fix false positive warnings on exhaust_iterables

3.12.0 (2022-04-13)
-------------------

* Add option to decode empty strings as ``""`` instead of ``None``.
  Option ``decode_empty_string_none`` of ``GrpcDecoder`` controls this behavior.
  Default is ``True`` (i.e. replace empty strings with ``None`` which is
  current behavior). This will change in the next major version.

3.11.1 (2022-04-12)
-------------------

* Reraise exception when exhausting async iterables in CallableMock

3.11.0 (2022-04-07)
-------------------

* Add option to exhaust (async) iterables in CallableMock
* Add option not to decode unset messages
* Add decoding of fields with explicit ``optional`` label

3.10.1 (2022-03-01)
-------------------

* Fix request annotations.
* Deprecate AsycMock automagical awaitables.

3.10.0 (2022-02-23)
-------------------

* Add support for async stream calls.

3.9.0 (2022-02-08)
------------------

* Drop support for python 3.6.
* Add support for python 3.10.
* Fix closed event loop (#14).
* Add sync proxy (#11).
* Add gRPC options to client (#13).
* Update static checks.
* Update mypy & annotations.

3.8.0 (2021-10-14)
------------------

 * Add credentials utility (#9).
 * Add class attrs to client (#1).
 * Update static checks.

3.7.1 (2021-09-08)
------------------

 * Fix typing in ``service`` utilities.

3.7.0 (2021-08-11)
------------------

 * Decode exceptions by type
   * Decoding exceptions by name is now DEPRECATED

3.6.0 (2021-08-03)
------------------

 * Add asynchronous gRPC client.
 * Support python 3.6-3.9.
 * Replace setup.py with setup.cfg
 * Update configuration of static checks.

3.5.1 (2021-05-05)
------------------

 * Fix decoding of ``Struct`` - don't decode empty strings as ``None``.

3.5.0 (2021-04-08)
------------------

 * Add utilities for service methods.
 * Allow decoding to any exception.
 * Cleanup tests and CI configuration.

3.4.0 (2020-11-24)
------------------

 * Add shallow fields.
 * Update pydocstyle settings
 * Rename ChangeLog.rst to CHANGELOG.rst

3.3.1 (2020-09-15)
------------------

 * Fix decoding of nested lists from ``Struct``.

3.3.0 (2020-08-04)
------------------

 * Rename private method for decoding messages.
 * Add decoder for ``Struct``.
 * Update timestamp decoding. Timestamp 0 is now decoded to ``None``.
 * Update CI & tests. Use python images for CI.

3.2.0 (2020-06-15)
------------------

 * Fix returned default values.
 * Fix decoding of oneof fields.
 * Refactor core to support various requests.
 * Split asynchronous calls to call_future.
 * Add support for streaming responses.
 * Add a test client mixin.
 * Support python 3.8 and new gRPC versions.
 * Add license

3.1.1 (2020-06-05)
------------------

 * Log requests on single line.
 * Test multiple grpcio vesions.

3.1.0 (2020-01-23)
------------------

 * Add retry if call fails.

3.0.0 (2020-01-15)
------------------

 * Fix exception management after switch to non-blocking calls.
 * Fix compatibility with grpcio > 1.24.
 * Fix docstrings.

2.2.0 (2019-12-09)
------------------

 * Add support to non-blocking calls.

2.1.0 (2019-11-20)
------------------

 * Improve logging
 * Fix type annotations
 * Update tox & CI

2.0.0 (2019-10-01)
------------------

 * Fix client destructor
 * Log reconnections
 * Install and CI updates

1.0.0 (2019-06-03)
------------------

* Initial version
