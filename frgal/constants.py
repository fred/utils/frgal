"""Frgal constants."""

from enum import Enum
from typing import Final, Literal

GRPC_MIN_RECONNECT_BACKOFF_MS = 100
GRPC_MAX_RECONNECT_BACKOFF_MS = 1500
MAX_CLIENT_CONCURRENCY = 20


class _Sentinel(Enum):
    """Sentinel for default argument values.

    Based on https://stackoverflow.com/a/60605919/2440346
    """

    DEFAULT = object()


DEFAULT: Final = _Sentinel.DEFAULT
DefaultType = Literal[_Sentinel.DEFAULT]
