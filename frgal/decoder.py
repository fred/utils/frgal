"""Frgal decoder."""

import warnings
from collections.abc import Iterable, Mapping, MutableMapping, MutableSequence
from datetime import datetime, timezone
from enum import Enum
from typing import Any, Callable, Optional, Union, cast

from google.protobuf.descriptor import FieldDescriptor
from google.protobuf.message import Message
from google.protobuf.struct_pb2 import ListValue, Struct
from google.protobuf.timestamp_pb2 import Timestamp

from .exceptions import DecoderError, UnknownError


class GrpcDecoder:
    """Generic grpc backend decoder.

    @ivar decoders: Dictionary of protobuf types and callables to decode them.
    @ivar exception_decoders: Dictionary of exception names and related exception classes.

    Decoding description:
        * Decoding is done recursively for all the fields defined in the message.
        * Oneof fields are decoded only when they have their values set.
        * Primitive types are not decoded.
        * Repeated fields are decoded as arrays.
        * Map fields are decoded as dictionaries.
        * Unset message fields are decoded as `None` regardless of their inner structure.
        * Messages with no fields, such as `Empty`, are decoded as `None`.
        * Messages with a single field are flattened and decoded to that field.
        * Other messages are decoded as dictionaries.
    """

    def __init__(self) -> None:
        """Set common message type decoders."""
        self.decoders: dict[Any, Callable] = {}
        self.exception_decoders: dict[Union[str, type[Message]], type[Exception]] = {}
        self.enum_decoders: dict[str, type[Enum]] = {}

        for typ in (bool, int, float, str, bytes, type(None)):
            self.set_decoder(typ, lambda x: x)  # pragma: no cover

        # There are two different GRPC types for lists depending on their content
        self.set_decoder(MutableSequence, self._decode_list)

        # There are two different GRPC types for maps depending on their content
        self.set_decoder(MutableMapping, self._decode_dict)

        # Struct and its utility ListValue.
        self.set_decoder(Struct, self._decode_struct)
        self.set_decoder(ListValue, self._decode_list_value)

        self.set_decoder(Timestamp, self._decode_timestamp)
        self.set_decoder(Message, self._decode_message)

    def set_decoder(self, object_type: Any, decode_func: Callable) -> None:
        """Set custom decoder.

        Args:
            object_type: Backend object type.
            decode_func (Callable): Callable getting backend value and returning decoded one.
        """
        self.decoders[object_type] = decode_func

    def set_exception_decoder(
        self,
        type: type[Message],
        exception: type[Exception],
    ) -> None:
        """Set custom exception.

        Args:
            type: Exception message type.
            exception: Python exception to be returned.
        """
        self.exception_decoders[type] = exception

    def set_enum_decoder(
        self,
        type: type[Message],
        enum: type[Enum],
    ) -> None:
        """Set custom exception.

        Args:
            type: Exception message type.
            enum: Enum used to decode the value.
        """
        if not issubclass(enum, (int, str)):
            raise ValueError("Enum must be either int or str subclass")
        self.enum_decoders[type.DESCRIPTOR.full_name] = enum

    def decode(self, value: Any) -> Any:
        """Decode backend value.

        If there is custom decoder for the value type or its ancestors, it's used.
        Otherwise, exception is raised.
        """
        for cls in type(value).mro():
            if cls in self.decoders:
                return self.decoders[cls](value)
        # XXX: Do another loop through decoders and try `isinstance` instead
        # because google protobufs have broken `mro`.
        # See https://github.com/protocolbuffers/protobuf/issues/11150
        for cls in self.decoders:
            if isinstance(value, cls):
                return self.decoders[cls](value)

        raise DecoderError("Cannot decode unknown type {}".format(type(value)))

    def decode_exception(self, value: Message) -> Exception:
        """Return appropriate exception type.

        Args:
            value: GRPC Exception Message
        """
        name = value.WhichOneof("Reasons")
        if not name:
            return UnknownError("Exception was returned with no reason defined.")

        exception = getattr(value, name)
        exc_type = type(exception)
        if exc_type in self.exception_decoders:
            details = self._decode_message_fields(exception) or {}
            return self.exception_decoders[exc_type](**details)
        else:
            return UnknownError(name)

    def _decode_list(self, value: Iterable) -> list:
        """Decode list."""
        return [self.decode(x) for x in value]

    def _decode_dict(self, value: Mapping) -> dict:
        """Decode dict."""
        # Keys are primitive types, so they don't need to be decoded.
        return {a: self.decode(b) for a, b in value.items()}

    def _decode_message(self, value: Message) -> Any:
        """Decode GRPC Message object.

        Message without any fields defined, such as Empty, is decoded to None.
        Message with only one field defined is decoded to the decoding that field.
        Otherwise, Message is decoded to dictionary.
          * Keys of this dictionary are all Message fields names, whether they're set or not, except for oneof fields.
          * Only explicitely set oneof fields are decoded.
        """
        possible_fields = value.DESCRIPTOR.fields
        if len(possible_fields) == 0:
            return None
        elif len(possible_fields) == 1:
            return self._decode_message_field(value, possible_fields[0])
        else:
            return self._decode_message_fields(value)

    def _decode_message_fields(self, value: Message) -> dict[str, Any]:
        """Decode GRPC Message object to dictionary."""
        possible_fields = value.DESCRIPTOR.fields
        decoded = {}  # type: dict[str, Any]

        for field in possible_fields:
            if (
                field.containing_oneof
                and not self._is_optional_field(field)
                and field.name != value.WhichOneof(field.containing_oneof.name)
            ):
                # Skip undefined oneof fields.
                continue
            decoded[field.name] = self._decode_message_field(value, field)
        return decoded

    def _decode_message_field(self, message: Message, field: FieldDescriptor) -> Any:
        """Decode message field."""
        try:
            if message.HasField(field.name):
                if field.type == field.TYPE_ENUM:
                    return self._decode_enum(message, field)
                return self.decode(getattr(message, field.name))
            else:
                return None  # Field is not set.

        except ValueError:
            # Field presence cannot be tested (non-optional, non-submessage), so we decode it.
            if field.type == field.TYPE_ENUM:
                return self._decode_enum(message, field)
            return self.decode(getattr(message, field.name))

    def _decode_enum(self, message: Message, field: FieldDescriptor) -> Union[Enum, int]:
        try:
            enum = self.enum_decoders[field.enum_type.full_name]
        except KeyError:
            # Decode enum as int
            warnings.warn(
                "Decoding Enum as int is deprecated and will be removed in the future.",
                DeprecationWarning,
                stacklevel=2,
            )
            return cast(int, getattr(message, field.name))
        else:
            try:
                if issubclass(enum, int):
                    # Try decoding by field number
                    return enum(getattr(message, field.name))
                elif issubclass(enum, str):
                    # Try decoding by field name
                    return enum(field.enum_type.values_by_number[getattr(message, field.name)].name)
            except KeyError as err:
                # Value is not defined in gRPC Enum
                raise DecoderError(f"Unknown value {getattr(message, field.name)} of enum {enum}") from err
            except ValueError as err:
                # Value is not defined in Python Enum
                raise DecoderError(str(err)) from err
            else:  # pragma: no cover
                raise DecoderError("Unknown enum type")

    @staticmethod
    def _is_optional_field(field: FieldDescriptor) -> bool:
        """Determine whether field has label `optional`.

        Optional fields are implemented as oneofs in grpc core.
        There is no direct way to test whether field is defined
        with `optional` keyword. However, the generated oneof's
        name is underscore followed by the field name. We use this
        to recognize `optional` fields until there is some better
        way to do it.
        """
        oneof = field.containing_oneof
        if oneof and len(oneof.fields) == 1 and oneof.name == f"_{field.name}":
            return True
        else:
            return False

    def _decode_struct_value(self, value: Any) -> Any:
        """Decode struct value.

        Utility method to decode only Struct related structures and leave other values intact.
        """
        if isinstance(value, (Struct, ListValue)):
            return self.decode(value)
        else:
            return value

    def _decode_struct(self, value: Struct) -> dict:
        """Decode google.protobuf.Struct to dict."""
        return {k: self._decode_struct_value(v) for k, v in value.items()}

    def _decode_list_value(self, value: ListValue) -> list:
        """Decode google.protobuf.ListValue to list.

        Utility method to decode ListValue, which represents a sequence within Struct.
        """
        items = value.items()  # type: ignore[func-returns-value] # Mypy doesn't guess correctly here
        return [self._decode_struct_value(v) for v in items]

    @staticmethod
    def _decode_timestamp(timestamp: Timestamp) -> Optional[datetime]:
        """Decode timestamp to aware datetime or None."""
        if not timestamp.seconds and not timestamp.nanos:
            return None
        return timestamp.ToDatetime().replace(tzinfo=timezone.utc)
