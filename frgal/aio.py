"""Asynchronous gRPC client support."""

import asyncio
import inspect
import logging
from asyncio import Semaphore
from collections.abc import AsyncIterable, AsyncIterator, Iterable
from functools import partial, singledispatch
from pathlib import Path
from typing import Any, ClassVar, Optional, Union, overload

import grpc.aio
from asgiref.sync import async_to_sync
from asyncstdlib import enumerate as aenumerate
from google.protobuf.empty_pb2 import Empty
from google.protobuf.message import Message
from grpc import ChannelCredentials
from grpc.aio import Channel

from ._utils import MESSAGE_MAX_COUNT, LogRequestStreamWrapper, get_call_id, lazy_format_message
from .client import make_credentials
from .constants import (
    DEFAULT,
    GRPC_MAX_RECONNECT_BACKOFF_MS,
    GRPC_MIN_RECONNECT_BACKOFF_MS,
    MAX_CLIENT_CONCURRENCY,
    DefaultType,
)
from .decoder import GrpcDecoder
from .utils import _consume_async_gen

_LOGGER = logging.getLogger(__name__)

Request = Optional[Union[Message, Iterable[Message], AsyncIterable[Message]]]


class AsyncGrpcClient:
    """Base class for asynchronous gRPC clients.

    Class Attributes:
        decoder_cls: A default decoder class. No arguments are provided for its instantiation.
        options: Custom options for the gRPC client.
        stub_cls: The service stub for this client.
        timeout: A default timeout for gRPC calls. Default value is `None`, i.e. no timeout.

    Attributes:
        decoder: The decoder.
        options: The final options of the gRPC client.
            The final options are merged from class and instance options, the later take precedence.
        stub_cls: The service stub for this client.
        timeout: Timeout for gRPC calls.
    """

    decoder_cls: ClassVar[type[GrpcDecoder]] = GrpcDecoder
    options: dict[str, Any] = {
        "grpc.min_reconnect_backoff_ms": GRPC_MIN_RECONNECT_BACKOFF_MS,
        "grpc.max_reconnect_backoff_ms": GRPC_MAX_RECONNECT_BACKOFF_MS,
    }
    stub_cls: Optional[type] = None
    timeout: Optional[float] = None

    @overload
    def __init__(
        self,
        netloc: str,
        *,
        decoder: Optional[GrpcDecoder] = None,
        credentials: Optional[ChannelCredentials] = None,
        options: Optional[dict[str, Any]] = None,
        timeout: Union[Optional[float], DefaultType] = DEFAULT,
        semaphore: Optional[Semaphore] = None,
        stub_cls: Optional[type] = None,
    ) -> None: ...

    @overload
    def __init__(
        self,
        netloc: str,
        *,
        decoder: Optional[GrpcDecoder] = None,
        ssl_cert: Optional[Union[str, Path]] = None,
        options: Optional[dict[str, Any]] = None,
        timeout: Union[Optional[float], DefaultType] = DEFAULT,
        semaphore: Optional[Semaphore] = None,
        stub_cls: Optional[type] = None,
    ) -> None: ...

    def __init__(
        self,
        netloc: str,
        *,
        decoder: Optional[GrpcDecoder] = None,
        credentials: Optional[ChannelCredentials] = None,
        ssl_cert: Optional[Union[str, Path]] = None,
        options: Optional[dict[str, Any]] = None,
        timeout: Union[Optional[float], DefaultType] = DEFAULT,
        semaphore: Optional[Semaphore] = None,
        stub_cls: Optional[type] = None,
    ) -> None:
        """Initialize GRPC backend client instance.

        Args:
            netloc: Network location of a gRPC server.
            decoder: Custom decoder instance.
            credentials: Credentials for a secure channel connection. If None, insecure channel is used.
                Mutually exclusive with `ssl_cert`.
            ssl_cert: Path to a file with SSL certificate. Mutually exclusive with `credentials`.
            options: Custom options for the gRPC client.
            stub_cls: Custom service stub for this client.
            timeout: Custom timeout for gRPC calls.
            semaphore: A semaphore instance. If `None` (default), a semaphore with value 20 is used.
        """
        self.netloc = netloc
        self.decoder = decoder or self.decoder_cls()

        if ssl_cert and credentials:
            raise ValueError("Only one of credentials and ssl_cert may be provided.")
        elif ssl_cert:
            self.credentials = make_credentials(ssl_cert)
        else:
            self.credentials = credentials

        self.options = dict(self.options, **(options or {}))
        self.stub_cls = stub_cls or self.stub_cls
        if self.stub_cls is None:
            raise ValueError("No stub_cls provided.")
        if timeout is not DEFAULT:
            self.timeout = timeout
        self.semaphore = semaphore or Semaphore(MAX_CLIENT_CONCURRENCY)

        self._channel: Optional[Channel] = None

        # Stubs cache
        self._stub: Any = None

    def _ensure_loop_not_closed(self) -> None:
        """Ensure that channel event loop is not closed."""
        if self._channel and self._channel._loop.is_closed():
            self._channel = None
            self._stub = None

    @property
    def channel(self) -> Channel:
        """Create channel to GPRC server."""
        self._ensure_loop_not_closed()

        if self._channel is not None:
            return self._channel

        if self.credentials is not None:
            self._channel = grpc.aio.secure_channel(self.netloc, self.credentials, self.options.items())
        else:
            self._channel = grpc.aio.insecure_channel(self.netloc, self.options.items())
        return self._channel

    def get_stub(self) -> Any:
        """Return stub for the service.

        Stubs are cached, so for each service, there is only one stub created.
        However, stubs are created from channel and have to be reinstantiated
        after reconnect.
        """
        self._ensure_loop_not_closed()
        if self._stub is None:
            assert self.stub_cls is not None  # noqa: S101
            self._stub = self.stub_cls(self.channel)
        return self._stub

    async def call(
        self,
        method: str,
        request: Request,
        *,
        timeout: Union[Optional[float], DefaultType] = DEFAULT,
        max_retries: int = 10,
    ) -> Any:
        """Call grpc method.

        Args:
            method: Name of service method.
            request: Request passed as an argument to service method.
            timeout: An optional duration in seconds allowed for the gRPC call.
            max_retries: A maximum number of calls to be made.
        """
        if timeout is DEFAULT:
            timeout = self.timeout
        call_id = get_call_id()
        _LOGGER.debug("[%s] %s started", call_id, method)

        if isinstance(request, (Iterable, AsyncIterable)):
            request = LogRequestStreamWrapper(request, _LOGGER, call_id, method)
        else:
            _LOGGER.debug("[%s] %s(%s)", call_id, method, lazy_format_message(request))

        async with self.semaphore:
            retries = 0
            try:
                while True:
                    await asyncio.sleep(0.1 * retries)  # Wait for increasing amount of time before each call
                    try:
                        retries += 1
                        stub = self.get_stub()
                        grpc_method = getattr(stub, method)
                        reply = await grpc_method(request, timeout=timeout)
                        break
                    except grpc.RpcError as error:
                        if error.code() == grpc.StatusCode.UNAVAILABLE:
                            if retries < max_retries:
                                _LOGGER.info("[%s] Connection failed. Attempting retry.", call_id)
                            else:
                                _LOGGER.info("[%s] Connection failed. Retries were unsuccessful.", call_id)
                                raise
                        else:
                            raise

            except grpc.RpcError as error:
                _LOGGER.info("[%s] %s failed with %s", call_id, method, error)
                raise

        _LOGGER.debug("[%s] %s returned %s", call_id, method, lazy_format_message(reply))
        return self.decode_reply(reply)

    async def call_stream(
        self,
        method: str,
        request: Request,
        *,
        timeout: Union[Optional[float], DefaultType] = DEFAULT,
    ) -> AsyncIterator[Any]:
        """Call grpc method returning stream.

        Args:
            method: Name of service method.
            request: Request passed as an argument to service method.
            timeout: An optional duration in seconds allowed for the gRPC call.
        """
        if timeout is DEFAULT:
            timeout = self.timeout
        call_id = get_call_id()
        _LOGGER.debug("[%s] stream %s started", call_id, method)

        if isinstance(request, (Iterable, AsyncIterable)):
            request = LogRequestStreamWrapper(request, _LOGGER, call_id, method)
        else:
            _LOGGER.debug("[%s] %s(%s)", call_id, method, lazy_format_message(request))

        async with self.semaphore:
            chunks = 0
            try:
                stub = self.get_stub()
                grpc_method = getattr(stub, method)
                async for i, reply in aenumerate(grpc_method(request, timeout=timeout)):  # pragma: no cover
                    if i < MESSAGE_MAX_COUNT:
                        _LOGGER.debug("[%s] %s returned %s", call_id, method, lazy_format_message(reply))
                    yield self.decode_reply(reply)
                    chunks += 1
            except grpc.RpcError as error:
                _LOGGER.info("[%s] stream %s failed after %s chunks with %s", call_id, method, chunks, error)
                raise

        _LOGGER.debug("[%s] stream %s returned %d chunks", call_id, method, chunks)

    def decode_reply(self, reply: Message) -> Optional[dict[str, Any]]:
        """Decode reply and return decoded result or raise decoded exception.

        Reply must either be Empty or contain at least one of fields `data` and `exception`.
        """
        # Assert the supported structure of messages
        assert (  # noqa: S101
            "exception" in reply.DESCRIPTOR.fields_by_name
            or "data" in reply.DESCRIPTOR.fields_by_name  # nosec
            or isinstance(reply, Empty)
        )

        # Decode exception first
        if "exception" in reply.DESCRIPTOR.fields_by_name and reply.HasField("exception"):
            raise self.decoder.decode_exception(reply.exception)  # type: ignore

        # Decode data otherwise
        if "data" in reply.DESCRIPTOR.fields_by_name and reply.HasField("data"):
            return self.decoder.decode(reply.data)  # type: ignore

        # The reply is empty
        return None


class SyncGrpcProxy:
    """Sync grpc proxy.

    This proxy can convert any async grpc client to synchronous client.
    """

    client_cls: ClassVar[type[AsyncGrpcClient]]

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        """Initialize sync proxy.

        Takes single argument `client` with instance of AsyncGrpcClient.
        Alternatively, you can specify `client_cls` class variable and provide
        __init__ with arguments to initialize this async grpc client.
        """

        # TODO: replace singledispatch with singledispatchmethod from Python 3.8
        @singledispatch
        def _get_client(*args: Any, **kwargs: Any) -> AsyncGrpcClient:
            return self.client_cls(*args, **kwargs)

        @_get_client.register(AsyncGrpcClient)
        def _(client: AsyncGrpcClient) -> AsyncGrpcClient:
            return client

        self.client = _get_client(*args, **kwargs)

    def __getattr__(self, name: str) -> Any:
        attr = getattr(self.client, name)
        if asyncio.iscoroutinefunction(attr):
            return async_to_sync(attr)
        elif inspect.isasyncgenfunction(attr):
            return async_to_sync(partial(_consume_async_gen, attr))
        else:
            return attr
