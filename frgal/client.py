"""Common GRPC backend client."""

import logging
import time
import warnings
from collections.abc import Generator, Iterable, Iterator
from pathlib import Path
from typing import Any, Callable, ClassVar, Optional, Union, overload

import grpc
from google.protobuf.empty_pb2 import Empty
from google.protobuf.message import Message
from grpc import Channel, ChannelCredentials, ssl_channel_credentials

from ._utils import MESSAGE_MAX_COUNT, LogRequestStreamWrapper, get_call_id, lazy_format_message
from .constants import DEFAULT, GRPC_MAX_RECONNECT_BACKOFF_MS, GRPC_MIN_RECONNECT_BACKOFF_MS, DefaultType
from .decoder import GrpcDecoder

_LOGGER = logging.getLogger(__name__)

Request = Optional[Union[Message, Iterable[Message]]]


class CallWrapper:
    """Wrapper for gRPC calls.

    Arguments:
        call: A gRPC method to be called.
        request: A gRPC request to be sent.
        client: A GrpcClient instance.
        timeout: Timeout for the call.
        max_retries: A maximum number of calls to be made.
        modify_result: A callable to modify the result of the gRPC method.
    """

    def __init__(
        self,
        call: Callable,
        request: Request,
        *,
        client: "GrpcClient",
        timeout: Optional[float] = None,
        max_retries: int = 10,
        modify_result: Optional[Callable[[Any], Any]] = None,
    ):
        self.call = call
        self.request = request
        self.client = client
        self.timeout = timeout
        self.max_retries = max_retries
        self.modify_result = modify_result
        self._call_id = get_call_id()

    def get_reply(self) -> Any:
        """Fetch and return the reply."""
        method = self.call._method.decode("utf-8")  # type: ignore
        if isinstance(self.request, Iterable):
            request: Request = LogRequestStreamWrapper(self.request, _LOGGER, self._call_id, method)
        else:
            request = self.request
            _LOGGER.debug("[%s] %s(%s)", self._call_id, method, lazy_format_message(request))

        return self.call(request, timeout=self.timeout)

    def _decode_reply(self, reply: Message) -> Any:
        """Actually decode reply and return decoded result or raise decoded exception."""
        # Assert the supported structure of messages
        assert (  # noqa: S101
            "exception" in reply.DESCRIPTOR.fields_by_name
            or "data" in reply.DESCRIPTOR.fields_by_name  # nosec
            or isinstance(reply, Empty)
        )

        modify_result = self.modify_result or (lambda x: x)

        # Decode exception first
        if "exception" in reply.DESCRIPTOR.fields_by_name and reply.HasField("exception"):
            raise self.client.decoder.decode_exception(reply.exception)  # type: ignore[attr-defined]

        # Decode data otherwise
        if "data" in reply.DESCRIPTOR.fields_by_name and reply.HasField("data"):
            return modify_result(self.client.decoder.decode(reply.data))  # type: ignore[attr-defined]

        # The reply is empty
        return modify_result(None)

    def decode_reply(self, reply: Message) -> Any:
        """Decode reply and return decoded result or raise decoded exception."""
        method = self.call._method.decode("utf-8")  # type: ignore
        _LOGGER.debug("[%s] %s returned %s", self._call_id, method, lazy_format_message(reply))
        return self._decode_reply(reply)

    def result(self) -> Any:
        """Return the call result."""
        # XXX: Extract the name of the gRPC method
        method = self.call._method.decode("utf-8")  # type: ignore
        _LOGGER.debug("[%s] %s started", self._call_id, method)
        retries = 0
        try:
            while True:
                time.sleep(0.1 * retries)  # Wait for increasing amount of time before each call
                try:
                    retries += 1
                    reply = self.get_reply()
                    break
                except grpc.RpcError as error:
                    if error.code() == grpc.StatusCode.UNAVAILABLE:
                        if retries < self.max_retries:
                            _LOGGER.info("[%s] Connection failed. Attempting retry.", self._call_id)
                        else:
                            _LOGGER.info("[%s] Connection failed. Retries were unsuccessful.", self._call_id)
                            raise
                    else:
                        raise
        except grpc.RpcError as error:
            _LOGGER.info("[%s] %s failed with %s", self._call_id, method, error)
            raise

        return self.decode_reply(reply)


class StreamCallWrapper(CallWrapper):
    """Wrapper for gRPC call with streaming response."""

    def decode_reply(self, reply: Iterable[Message]) -> Generator[Any, None, None]:  # type: ignore[override]
        """Decode replies one by one."""
        method = self.call._method.decode("utf-8")  # type: ignore
        chunks = 0
        for i, item in enumerate(reply):
            if i < MESSAGE_MAX_COUNT:
                _LOGGER.debug("[%s] %s returned %s", self._call_id, method, lazy_format_message(item))
            yield self._decode_reply(item)
            chunks += 1
        _LOGGER.debug("[%s] %s returned %d chunks", self._call_id, method, chunks)


class FutureCallWrapper(CallWrapper):
    """Wrapper for gRPC future call."""

    def __init__(
        self,
        call: Callable,
        request: Request,
        *,
        client: "GrpcClient",
        timeout: Optional[float] = None,
        max_retries: int = 10,
        modify_result: Optional[Callable[[Any], Any]] = None,
    ):
        super().__init__(
            call, request, client=client, timeout=timeout, max_retries=max_retries, modify_result=modify_result
        )
        # Get a future object - start an asynchronous request
        self._future = self._call_future()

    def _call_future(self) -> grpc.Future:
        """Start asynchronous request and return a future object."""
        future_call = self.call.future  # type: ignore
        return future_call(self.request, timeout=self.timeout)

    def get_reply(self) -> Any:
        """Fetch and return the reply."""
        return self._future.result()

    def __getattr__(self, name: str) -> Any:
        """Proxy all other future properties."""
        return getattr(self._future, name)


class GrpcClient:
    """GRPC backend client.

    Class Attributes:
        decoder_cls: A default decoder class. No arguments are provided for its instantiation.
        options: Custom options for the gRPC client.
        stub_cls: The service stub for this client.
        timeout: A default timeout for gRPC calls. Default value is `None`, i.e. no timeout.

    Attributes:
        decoder: The decoder.
        options: The final options of the gRPC client.
            The final options are merged from class and instance options, the later take precedence.
        stub_cls: The service stub for this client.
        timeout: Timeout for gRPC calls.
    """

    decoder_cls: ClassVar[type[GrpcDecoder]] = GrpcDecoder
    options: dict[str, Any] = {
        "grpc.min_reconnect_backoff_ms": GRPC_MIN_RECONNECT_BACKOFF_MS,
        "grpc.max_reconnect_backoff_ms": GRPC_MAX_RECONNECT_BACKOFF_MS,
    }
    stub_cls: Optional[type] = None
    timeout: Optional[float] = None

    @overload
    def __init__(
        self,
        netloc: str,
        *,
        decoder: Optional[GrpcDecoder] = None,
        credentials: Optional[ChannelCredentials] = None,
        options: Optional[dict[str, Any]] = None,
        timeout: Union[Optional[float], DefaultType] = DEFAULT,
        stub_cls: Optional[type] = None,
    ): ...

    @overload
    def __init__(
        self,
        netloc: str,
        *,
        decoder: Optional[GrpcDecoder] = None,
        ssl_cert: Optional[Union[str, Path]] = None,
        options: Optional[dict[str, Any]] = None,
        timeout: Union[Optional[float], DefaultType] = DEFAULT,
        stub_cls: Optional[type] = None,
    ): ...

    def __init__(
        self,
        netloc: str,
        *,
        decoder: Optional[GrpcDecoder] = None,
        credentials: Optional[ChannelCredentials] = None,
        ssl_cert: Optional[Union[str, Path]] = None,
        options: Optional[dict[str, Any]] = None,
        timeout: Union[Optional[float], DefaultType] = DEFAULT,
        stub_cls: Optional[type] = None,
    ):
        """Initialize GRPC backend client instance.

        Args:
            netloc: Network location of a gRPC server.
            decoder: Custom decoder instance.
            credentials: Credentials for a secure channel connection. If None, insecure channel is used.
                Mutually exclusive with `ssl_cert`.
            ssl_cert: Path to a file with SSL certificate. Mutually exclusive with `credentials`.
            options: Custom options for the gRPC client.
            stub_cls: Custom service stub for this client.
            timeout: Custom timeout for gRPC calls.
        """
        self.netloc = netloc
        self.decoder = decoder or self.decoder_cls()

        if ssl_cert and credentials:
            raise ValueError("Only one of credentials and ssl_cert may be provided.")
        elif ssl_cert:
            self.credentials = make_credentials(ssl_cert)
        else:
            self.credentials = credentials

        self.options = dict(self.options, **(options or {}))
        self.stub_cls = stub_cls or self.stub_cls
        if self.stub_cls is None:
            raise ValueError("No stub_cls provided.")
        if timeout is not DEFAULT:
            self.timeout = timeout

        self._channel: Optional[Channel] = None

        # Stubs cache
        self._stub: Any = None

    @property
    def channel(self) -> Channel:
        """Create channel to GPRC server."""
        if self._channel is not None:
            return self._channel

        if self.credentials is not None:
            self._channel = grpc.secure_channel(self.netloc, self.credentials, self.options.items())
        else:
            self._channel = grpc.insecure_channel(self.netloc, self.options.items())
        return self._channel

    def get_stub(self) -> Any:
        """Return stub for the service.

        Stubs are cached, so for each service, there is only one stub created.
        """
        if self._stub is None:
            assert self.stub_cls is not None  # noqa: S101
            self._stub = self.stub_cls(self.channel)
        return self._stub

    def call(
        self,
        method: str,
        request: Request,
        *,
        timeout: Union[Optional[float], DefaultType] = DEFAULT,
        max_retries: int = 10,
        modify_result: Optional[Callable[[Any], Any]] = None,
    ) -> Any:
        """Call grpc method.

        Args:
            method: Name of service method.
            request: Request passed as an argument to service method.
            timeout: An optional duration in seconds allowed for the gRPC call.
            max_retries: A maximum number of calls to be made.
            modify_result: Callable that processes decoded result and modifies it. Default is identity.
        """
        if timeout is DEFAULT:
            timeout = self.timeout
        stub = self.get_stub()
        grpc_method = getattr(stub, method)
        wrapper = CallWrapper(
            grpc_method, request, client=self, timeout=timeout, max_retries=max_retries, modify_result=modify_result
        )
        return wrapper.result()

    def call_future(
        self,
        method: str,
        request: Request,
        *,
        timeout: Union[Optional[float], DefaultType] = DEFAULT,
        max_retries: int = 10,
        modify_result: Optional[Callable[[Any], Any]] = None,
    ) -> FutureCallWrapper:
        """Call grpc method asynchronously.

        Args:
            method: Name of service method.
            request: Request passed as an argument to service method.
            timeout: An optional duration in seconds allowed for the gRPC call.
            max_retries: A maximum number of calls to be made.
            modify_result: Callable that processes decoded result and modifies it. Default is identity.
        """
        warnings.warn(
            "Method call_future is deprecated and will be removed. Use async instead.", DeprecationWarning, stacklevel=2
        )
        if timeout is DEFAULT:
            timeout = self.timeout
        stub = self.get_stub()
        grpc_method = getattr(stub, method)
        return FutureCallWrapper(
            grpc_method, request, client=self, timeout=timeout, max_retries=max_retries, modify_result=modify_result
        )

    def call_stream(
        self,
        method: str,
        request: Request,
        *,
        timeout: Union[Optional[float], DefaultType] = DEFAULT,
        max_retries: int = 10,
        modify_result: Optional[Callable[[Any], Any]] = None,
    ) -> Iterator[Any]:
        """Call grpc method with streaming response.

        Args:
            method: Name of service method.
            request: Request passed as an argument to service method.
            timeout: An optional duration in seconds allowed for the gRPC call.
            max_retries: A maximum number of calls to be made.
            modify_result: Callable that processes decoded result and modifies it. Default is identity.
        """
        if timeout is DEFAULT:
            timeout = self.timeout
        stub = self.get_stub()
        grpc_method = getattr(stub, method)
        wrapper = StreamCallWrapper(
            grpc_method, request, client=self, timeout=timeout, max_retries=max_retries, modify_result=modify_result
        )
        return wrapper.result()  # type: ignore[no-any-return]


def make_credentials(ssl_cert: Optional[Union[str, Path]]) -> Optional[ChannelCredentials]:
    """Create client-side credentials from SSL certificate file.

    Args:
        ssl_cert: Path to a SSL certificate file.

    Returns:
        None if ssl_cert is None.
        Credentials otherwise.

    Raises:
        OSError: If an error on SSL certificate file is encountered.
    """
    if ssl_cert is not None:
        with open(ssl_cert) as file:
            return ssl_channel_credentials(file.read())
    else:
        return None
