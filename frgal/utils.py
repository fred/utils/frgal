"""Various utilities."""

import asyncio
import warnings
from collections.abc import AsyncIterable, Awaitable, Iterable, Sequence
from functools import partial
from threading import Thread
from typing import Any, Callable, ClassVar, Optional, TypeVar
from unittest.mock import AsyncMock, AsyncMock as UnittestAsyncMock, Mock, _Call

import grpc
from asyncstdlib import list as alist

from ._utils import LogRequestStreamWrapper

T = TypeVar("T")


def make_awaitable(value: T) -> Awaitable[T]:
    """Convert value to an awaitable."""

    async def _wrapper() -> T:
        return value

    return _wrapper()


class CallableMock:
    """Mock of a gRPC method Callable object."""

    def __init__(self, method: str, call: Callable, *, exhaust_iterables: Optional[bool] = None):
        self._method = method.encode("utf-8")
        self.call = call
        self.exhaust_iterables = exhaust_iterables

    def __call__(self, request: Any, *args: Any, **kwargs: Any) -> Any:
        """Mock gRPC method call."""
        if self.exhaust_iterables:
            request = self._exhaust(request)
        elif (
            self.exhaust_iterables is None
            and not isinstance(request, Sequence)
            and isinstance(request, (Iterable, AsyncIterable))
        ):
            warnings.warn(
                "Default value of `exhaust_iterables` will change to True in next major release."
                "If you want to maintain the current behaviour, set it to False explicitely.",
                DeprecationWarning,
                stacklevel=2,
            )

        return self.call(request, *args, **kwargs)

    def future(self, *args: Any, **kwargs: Any) -> Mock:
        """Mock asynchronous gRPC method call."""
        future_mock = Mock(name="future", spec=("result",))
        future_mock.result.return_value = self.call(*args, **kwargs)
        return future_mock

    @staticmethod
    def _exhaust(value: Any) -> Any:
        """Exhaust any iterables."""
        results: list[Any] = []
        exceptions: list[Exception] = []

        def _inner(value: AsyncIterable, results: list[Any], exceptions: list[Exception]) -> Any:
            try:
                results.append(tuple(asyncio.run(alist(value))))
            except Exception as err:
                exceptions.append(err)

        # Unwrap LogRequestStreamWrapper.
        if isinstance(value, LogRequestStreamWrapper):
            value = value.requests

        if isinstance(value, Sequence):
            # Strings, lists and tuples should be left untouched
            return value
        elif isinstance(value, Iterable):
            return tuple(value)
        elif isinstance(value, AsyncIterable):
            # Run async iterable in separate thead – this works both
            # with and without running async loop in the main thread
            thread = Thread(target=_inner, args=(value, results, exceptions))
            thread.start()
            thread.join()
            if exceptions:
                raise exceptions[0]
            else:
                return results[0]
        else:
            return value


class MockingChannel(grpc.Channel):
    """A channel which binds Stub calls to TestClient internal mock."""

    def __init__(self, client: "TestClientMixin", exhaust_iterables: Optional[bool] = None):
        self.client = client
        self.exhaust_iterables = exhaust_iterables

    def subscribe(self, *args: Any, **kwargs: Any) -> None:
        """Noop."""

    def unsubscribe(self, *args: Any, **kwargs: Any) -> None:
        """Noop."""

    def unary_unary(self, method: str, *args: Any, **kwargs: Any) -> CallableMock:
        """Return a mocked method bound to the test client mock."""
        return CallableMock(method, partial(self.client.mock, method=method), exhaust_iterables=self.exhaust_iterables)

    def unary_stream(self, method: str, *args: Any, **kwargs: Any) -> CallableMock:
        """Return a mocked method bound to the test client mock."""
        return CallableMock(method, partial(self.client.mock, method=method), exhaust_iterables=self.exhaust_iterables)

    def stream_unary(self, method: str, *args: Any, **kwargs: Any) -> CallableMock:
        """Return a mocked method bound to the test client mock."""
        return CallableMock(method, partial(self.client.mock, method=method), exhaust_iterables=self.exhaust_iterables)

    def stream_stream(self, method: str, *args: Any, **kwargs: Any) -> CallableMock:
        """Return a mocked method bound to the test client mock."""
        return CallableMock(method, partial(self.client.mock, method=method), exhaust_iterables=self.exhaust_iterables)

    def close(self, *args: Any, **kwargs: Any) -> None:
        """Noop."""


class TestClientMixin:
    """Mixin to wrap gRPC client for tests.

    Attributes:
        mock: A mock which handles all gRPC method calls.
              Acts with a signature `(request: grpc.Message, method: str, timeout: int = None)`.
    """

    exhaust_iterables: ClassVar[Optional[bool]] = None

    def __init__(self, *args: Any, **kwargs: Any):
        super().__init__(*args, **kwargs)
        self.mock = Mock(spec=("__call__",))
        self._channel = MockingChannel(self, exhaust_iterables=self.exhaust_iterables)


class AsyncMockingChannel(grpc.Channel):
    """A channel which binds Stub calls to AsyncTestClient internal mock."""

    def __init__(self, client: "AsyncTestClientMixin", exhaust_iterables: Optional[bool] = None):
        self.client = client
        self.exhaust_iterables = exhaust_iterables

    def subscribe(self, *args: Any, **kwargs: Any) -> None:
        """Noop."""

    def unsubscribe(self, *args: Any, **kwargs: Any) -> None:
        """Noop."""

    def unary_unary(self, method: str, *args: Any, **kwargs: Any) -> CallableMock:
        """Return a mocked method bound to the test client mock."""
        return CallableMock(method, partial(self.client.mock, method=method), exhaust_iterables=self.exhaust_iterables)

    def unary_stream(self, method: str, *args: Any, **kwargs: Any) -> CallableMock:
        """Return a mocked method bound to the test client mock."""
        return CallableMock(
            method, partial(self.client.stream_mock, method=method), exhaust_iterables=self.exhaust_iterables
        )

    def stream_unary(self, method: str, *args: Any, **kwargs: Any) -> CallableMock:
        """Return a mocked method bound to the test client mock."""
        return CallableMock(method, partial(self.client.mock, method=method), exhaust_iterables=self.exhaust_iterables)

    def stream_stream(self, method: str, *args: Any, **kwargs: Any) -> CallableMock:
        """Return a mocked method bound to the test client mock."""
        return CallableMock(
            method, partial(self.client.stream_mock, method=method), exhaust_iterables=self.exhaust_iterables
        )

    def close(self, *args: Any, **kwargs: Any) -> None:
        """Noop."""


class AsyncTestClientMixin:
    """Mixin to wrap async gRPC client for tests.

    Attributes:
        mock: An async mock which handles all gRPC method calls with unary responses.
              Acts with a signature `(request: grpc.Message, method: str, timeout: int = None)`.
        stream_mock: An async mock which handles all gRPC method calls with stream responses.
              Acts with a signature `(request: grpc.Message, method: str, timeout: int = None)`.
    """

    exhaust_iterables: ClassVar[Optional[bool]] = None

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        self.mock = AsyncMock(spec=("__call__",), name="mock")
        self.stream_mock = Mock(spec=("__call__",), name="stream_mock")
        self._channel = AsyncMockingChannel(self, exhaust_iterables=self.exhaust_iterables)  # type: Any
        self._channel._loop = asyncio.new_event_loop()

    def __del__(self) -> None:
        self._channel._loop.close()


async def _consume_async_gen(gen: Callable[..., AsyncIterable], *args: Any, **kwargs: Any) -> list:
    return await alist(gen(*args, **kwargs))


def get_mock_awaits(mock: UnittestAsyncMock) -> list[_Call]:
    """Return a list of awaited call from mock and all its children.

    Awaits are in an order of their coroutine calls.
    """
    # A list of awaited calls.
    awaits = []
    # A queue of mock objects to process.
    queue: list[tuple[str, Mock]] = []
    queue.append(("", mock))
    # Collect awaits from the structure of mocks.
    # Based on `CallableMixin._increment_mock_call`.
    while queue:
        parent_name, a_mock = queue.pop(0)

        # Collect children to process.
        children = []
        if isinstance(a_mock._mock_return_value, Mock):
            children.append(a_mock._mock_return_value)
        children.extend(a_mock._mock_children.values())

        # Enqueue children.
        for child in children:
            # Create a new parent name for the child.
            if child._mock_new_name == "()":
                dot = ""
            else:
                dot = "."
            if parent_name:
                new_name = parent_name + dot + child._mock_new_name
            else:
                new_name = child._mock_new_name

            queue.append((new_name, child))

        # Collect calls for the current mock.
        for call in a_mock._mock_await_args_list:
            new_name = parent_name + (call._mock_name or "")
            new_call = _Call((new_name,) + call._get_call_arguments())
            awaits.append(new_call)

    result = []
    for call in mock.mock_calls:
        if call in awaits:
            awaits.remove(call)
            result.append(call)
    return result
