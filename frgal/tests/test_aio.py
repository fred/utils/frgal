"""Tests for aio module."""

import asyncio
import types
from collections.abc import AsyncIterable, Iterable
from functools import partial
from typing import Any, Callable
from unittest import IsolatedAsyncioTestCase as TestCase
from unittest.mock import AsyncMock, Mock, call, patch, sentinel

import grpc
from asyncstdlib import list as alist, tuple as atuple
from google.protobuf.empty_pb2 import Empty
from grpc.aio import Channel
from testfixtures import LogCapture, SequenceComparison

from frgal.aio import AsyncGrpcClient, SyncGrpcProxy
from frgal.constants import GRPC_MAX_RECONNECT_BACKOFF_MS, GRPC_MIN_RECONNECT_BACKOFF_MS
from frgal.decoder import GrpcDecoder
from frgal.exceptions import UnknownError
from frgal.utils import AsyncTestClientMixin, get_mock_awaits, make_awaitable

from .proto.example_pb2 import Reply, SimpleMessage  # type: ignore[attr-defined]
from .utils import make_reply

try:
    from grpc._channel import _MultiThreadedRendezvous as _Rendezvous
except ImportError:  # pragma: no cover
    from grpc._channel import _Rendezvous


async def aiter(iterable):
    for x in iterable:
        yield x


class AsyncGrpcClientTest(TestCase):
    """Test AsyncGrpcClient."""

    default_options = (
        ("grpc.min_reconnect_backoff_ms", GRPC_MIN_RECONNECT_BACKOFF_MS),
        ("grpc.max_reconnect_backoff_ms", GRPC_MAX_RECONNECT_BACKOFF_MS),
    )

    def setUp(self):
        self.stub_mock = Mock()
        self.grpc_module_mock = Mock(spec=("coffeeStub",))
        self.channel_mock = AsyncMock(spec=Channel)
        self.channel_mock._loop = asyncio.new_event_loop()

        with patch("grpc.aio.insecure_channel", return_value=self.channel_mock):
            self.client = AsyncGrpcClient(sentinel.netloc, stub_cls=self.stub_mock)
            self.client.channel

        self.log_handler = LogCapture("frgal.aio")
        self.addCleanup(self.log_handler.uninstall)

    def tearDown(self):
        self.client._channel._loop.close()  # type: ignore

    def test_init_no_stub_cls(self):
        with self.assertRaisesRegex(ValueError, "No stub_cls provided."):
            AsyncGrpcClient(sentinel.netloc)

    def test_channel_insecure(self):
        client = AsyncGrpcClient(sentinel.netloc, stub_cls=sentinel.stub)
        with patch("grpc.aio.insecure_channel", return_value=self.channel_mock) as mock:
            client.channel

        self.assertEqual(client.channel, self.channel_mock)
        self.assertEqual(mock.mock_calls, [call(sentinel.netloc, SequenceComparison(*self.default_options))])

    def test_channel_insecure_options(self):
        client = AsyncGrpcClient(sentinel.netloc, stub_cls=sentinel.stub, options={"key": sentinel.value})
        with patch("grpc.aio.insecure_channel", return_value=self.channel_mock) as mock:
            client.channel

        self.assertEqual(client.channel, self.channel_mock)
        options = dict(self.default_options, key=sentinel.value)
        self.assertEqual(mock.mock_calls, [call(sentinel.netloc, SequenceComparison(*options.items()))])

    def test_channel_secure(self):
        client = AsyncGrpcClient(sentinel.netloc, stub_cls=sentinel.stub, credentials=sentinel.credentials)
        with patch("grpc.aio.secure_channel", return_value=self.channel_mock) as mock:
            client.channel

        self.assertEqual(client.channel, self.channel_mock)
        self.assertEqual(
            mock.mock_calls, [call(sentinel.netloc, sentinel.credentials, SequenceComparison(*self.default_options))]
        )

    def test_channel_secure_options(self):
        client = AsyncGrpcClient(
            sentinel.netloc, stub_cls=sentinel.stub, credentials=sentinel.credentials, options={"key": sentinel.value}
        )
        with patch("grpc.aio.secure_channel", return_value=self.channel_mock) as mock:
            client.channel

        self.assertEqual(client.channel, self.channel_mock)
        options = dict(self.default_options, key=sentinel.value)
        self.assertEqual(
            mock.mock_calls, [call(sentinel.netloc, sentinel.credentials, SequenceComparison(*options.items()))]
        )

    def test_ssl_cert(self):
        with patch("frgal.aio.make_credentials", return_value=sentinel.credentials) as mock_credentials:
            client = AsyncGrpcClient(sentinel.netloc, stub_cls=sentinel.stub, ssl_cert=sentinel.ssl_cert)
        with patch("grpc.aio.secure_channel", return_value=self.channel_mock) as mock_channel:
            client.channel

        self.assertEqual(client.channel, self.channel_mock)
        self.assertEqual(mock_credentials.mock_calls, [call(sentinel.ssl_cert)])
        self.assertEqual(
            mock_channel.mock_calls,
            [call(sentinel.netloc, sentinel.credentials, SequenceComparison(*self.default_options))],
        )

    def test_credentials_ssl_cert_conflict(self):
        with self.assertRaisesRegex(ValueError, "Only one of credentials and ssl_cert may be provided"):
            AsyncGrpcClient(  # type: ignore[call-overload]
                sentinel.netloc,
                stub_cls=sentinel.stub,
                ssl_cert=sentinel.ssl_cert,
                credentials=sentinel.credentials,
            )

    def test_get_stub(self):
        stub_mock = Mock(return_value=sentinel.coffee_stub)

        client = AsyncGrpcClient(sentinel.netloc, stub_cls=stub_mock)

        with patch("grpc.aio.insecure_channel", return_value=self.channel_mock):
            stub = client.get_stub()

        self.assertEqual(stub, sentinel.coffee_stub)
        self.assertEqual(stub_mock.mock_calls, [call(self.channel_mock)])

    def test_get_stub_cache(self):
        # Test stubs are cached when using the same channel
        stub_mock = Mock(return_value=sentinel.coffee_stub)

        client = AsyncGrpcClient(sentinel.netloc, stub_cls=stub_mock)

        # Get stub once
        with patch("grpc.aio.insecure_channel", return_value=self.channel_mock):
            client.get_stub()
        # Get stub twice
        self.assertEqual(client.get_stub(), sentinel.coffee_stub)
        # Check it was created only once
        self.assertEqual(stub_mock.mock_calls, [call(self.channel_mock)])

    def test_get_stub_closed_event_loop(self):
        # Test stubs are cached when using the same channel
        self.stub_mock.return_value = sentinel.coffee_stub

        # Get stub once
        self.client.get_stub()

        # Close event loop and prepare new channel
        self.client._channel._loop.close()  # type: ignore
        channel_mock = AsyncMock(spec=Channel)
        channel_mock._loop = asyncio.new_event_loop()

        with patch("grpc.aio.insecure_channel", return_value=channel_mock) as channel:
            # Get stub second time
            self.assertEqual(self.client.get_stub(), sentinel.coffee_stub)

        self.assertEqual(channel.mock_calls, [call(sentinel.netloc, SequenceComparison(*self.default_options))])
        self.assertEqual(
            self.stub_mock.mock_calls,
            [
                call.coffeeStub(self.channel_mock),
                call.coffeeStub(channel_mock),
            ],
        )

    async def test_call(self):
        data: Iterable[tuple[tuple, dict]] = (
            # args, kwargs
            (("make", sentinel.request), {}),
            (("make",), {"request": sentinel.request}),
            ((), {"method": "make", "request": sentinel.request}),
        )
        for args, kwargs in data:
            with self.subTest(args=args, kwargs=kwargs):
                stub_mock = Mock()
                with patch("grpc.aio.insecure_channel", return_value=self.channel_mock):
                    client = AsyncGrpcClient(sentinel.netloc, stub_cls=stub_mock)
                    client.channel
                reply = Reply()
                reply.data.result = "Espresso!"
                stub_mock.return_value.attach_mock(AsyncMock(return_value=reply), "make")

                self.assertEqual(await client.call(*args, **kwargs), "Espresso!")

                self.assertEqual(get_mock_awaits(stub_mock.return_value.make), [call(sentinel.request, timeout=None)])

    async def test_call_timeout(self):
        data: Iterable[tuple[dict, dict, Any]] = (
            # client_kwargs, call_kwargs, timeout
            ({}, {}, None),
            ({"timeout": sentinel.timeout}, {}, sentinel.timeout),
            ({}, {"timeout": sentinel.timeout}, sentinel.timeout),
            ({"timeout": sentinel.default}, {"timeout": sentinel.timeout}, sentinel.timeout),
        )
        for client_kwargs, call_kwargs, timeout in data:
            with self.subTest(client_kwargs=client_kwargs, call_kwargs=call_kwargs):
                stub_mock = Mock()
                with patch("grpc.aio.insecure_channel", return_value=self.channel_mock):
                    client = AsyncGrpcClient(sentinel.netloc, stub_cls=stub_mock, **client_kwargs)
                    client.channel
                reply = Reply()
                reply.data.result = "Espresso!"
                stub_mock.return_value.attach_mock(AsyncMock(return_value=reply), "make")

                self.assertEqual(await client.call("make", Empty(), **call_kwargs), "Espresso!")

                self.assertEqual(get_mock_awaits(stub_mock.return_value.make), [call(Empty(), timeout=timeout)])

    async def test_call_closed_event_loop(self):
        self.client.channel._loop.close()
        reply = Reply()
        reply.data.result = "Espresso!"
        self.stub_mock.return_value.make.return_value = make_awaitable(reply)
        channel_mock = AsyncMock(spec=Channel)
        channel_mock._loop = asyncio.new_event_loop()

        with patch("grpc.aio.insecure_channel", return_value=channel_mock) as channel_mock:
            with patch("frgal.aio.get_call_id", return_value="XXXX"):
                self.assertEqual(await self.client.call("make", Empty()), "Espresso!")

        self.assertEqual(channel_mock.mock_calls, [call(sentinel.netloc, SequenceComparison(*self.default_options))])
        self.log_handler.check(
            ("frgal.aio", "DEBUG", "[XXXX] make started"),
            ("frgal.aio", "DEBUG", "[XXXX] make()"),
            ("frgal.aio", "DEBUG", '[XXXX] make returned data { result: "Espresso!" }'),
        )

    async def test_call__empty_empty(self):
        self.stub_mock.return_value.make.return_value = make_awaitable(Empty())

        with patch("frgal.aio.get_call_id", return_value="XXXX"):
            self.assertEqual(await self.client.call("make", Empty()), None)

        self.log_handler.check(
            ("frgal.aio", "DEBUG", "[XXXX] make started"),
            ("frgal.aio", "DEBUG", "[XXXX] make()"),
            ("frgal.aio", "DEBUG", "[XXXX] make returned "),
        )

    async def test_call__unary_unary(self):
        request = SimpleMessage(value="Answer!")
        self.stub_mock.return_value.make.return_value = make_awaitable(make_reply("42"))

        with patch("frgal.aio.get_call_id", return_value="XXXX"):
            self.assertEqual(await self.client.call("make", request), "42")

        self.log_handler.check(
            ("frgal.aio", "DEBUG", "[XXXX] make started"),
            ("frgal.aio", "DEBUG", '[XXXX] make(value: "Answer!")'),
            ("frgal.aio", "DEBUG", '[XXXX] make returned data { result: "42" }'),
        )

    async def test_call__stream_unary(self):
        requests = iter((SimpleMessage(value="Lister"), SimpleMessage(value="Rimmer"), SimpleMessage(value="Kryten")))

        # Mock method to consumer the requests
        async def _make(requests, *args, **kwargs):
            await atuple(requests)
            return make_reply("Red Dwarf")

        self.stub_mock.return_value.make.side_effect = _make

        with patch("frgal.aio.get_call_id", return_value="XXXX"):
            self.assertEqual(await self.client.call("make", requests), "Red Dwarf")

        self.log_handler.check(
            ("frgal.aio", "DEBUG", "[XXXX] make started"),
            ("frgal.aio", "DEBUG", '[XXXX] make(value: "Lister")'),
            ("frgal.aio", "DEBUG", '[XXXX] make(value: "Rimmer")'),
            ("frgal.aio", "DEBUG", '[XXXX] make(value: "Kryten")'),
            ("frgal.aio", "DEBUG", '[XXXX] make returned data { result: "Red Dwarf" }'),
        )

    async def test_call_error(self):
        reply = Reply()
        reply.exception.tea_pot_error.CopyFrom(Reply.Exception.TeaPotError())
        self.stub_mock.return_value.make.return_value = make_awaitable(reply)

        with self.assertRaisesRegex(UnknownError, "tea_pot_error"):
            await self.client.call("make", Empty())

    async def test_call_grpc_unavailable(self):
        state = grpc._channel._RPCState((), "", "", grpc.StatusCode.UNAVAILABLE, "Connection reset by peer")
        error = _Rendezvous(state, None, None, None)
        reply = Reply()
        reply.data.result = "Espresso!"
        self.stub_mock.return_value.make.side_effect = [error, error, error, make_awaitable(reply)]

        with patch("frgal.aio.get_call_id", return_value="XXXX"):
            self.assertEqual(await self.client.call("make", Empty()), "Espresso!")

        calls = [
            call(self.channel_mock),
            call().make(Empty(), timeout=None),
            call().make(Empty(), timeout=None),
            call().make(Empty(), timeout=None),
            call().make(Empty(), timeout=None),
        ]
        self.assertEqual(self.stub_mock.mock_calls, calls)

        self.log_handler.check(
            ("frgal.aio", "DEBUG", "[XXXX] make started"),
            ("frgal.aio", "DEBUG", "[XXXX] make()"),
            ("frgal.aio", "INFO", "[XXXX] Connection failed. Attempting retry."),
            ("frgal.aio", "INFO", "[XXXX] Connection failed. Attempting retry."),
            ("frgal.aio", "INFO", "[XXXX] Connection failed. Attempting retry."),
            ("frgal.aio", "DEBUG", '[XXXX] make returned data { result: "Espresso!" }'),
        )

    async def test_call_grpc_unavailable_too_many_times(self):
        state = grpc._channel._RPCState((), "", "", grpc.StatusCode.UNAVAILABLE, "Connection reset by peer")
        error = _Rendezvous(state, None, None, None)
        reply = Reply()
        reply.data.result = "Espresso!"
        self.stub_mock.return_value.make.side_effect = error

        with patch("frgal.aio.get_call_id", return_value="XXXX"):
            with self.assertRaises(grpc.RpcError):
                await self.client.call("make", Empty(), max_retries=3)

        calls = [
            call(self.channel_mock),
            call().make(Empty(), timeout=None),
            call().make(Empty(), timeout=None),
            call().make(Empty(), timeout=None),
        ]
        self.assertEqual(self.stub_mock.mock_calls, calls)

        self.log_handler.check(
            ("frgal.aio", "DEBUG", "[XXXX] make started"),
            ("frgal.aio", "DEBUG", "[XXXX] make()"),
            ("frgal.aio", "INFO", "[XXXX] Connection failed. Attempting retry."),
            ("frgal.aio", "INFO", "[XXXX] Connection failed. Attempting retry."),
            ("frgal.aio", "INFO", "[XXXX] Connection failed. Retries were unsuccessful."),
            (
                "frgal.aio",
                "INFO",
                "[XXXX] make failed with <_MultiThreadedRendezvous of RPC that terminated with:\n"
                "\tstatus = StatusCode.UNAVAILABLE\n"
                '\tdetails = "Connection reset by peer"\n'
                '\tdebug_error_string = "None"\n'
                ">",
            ),
        )

    async def test_call_grpc_other_error(self):
        state = grpc._channel._RPCState((), "", "", grpc.StatusCode.UNKNOWN, "Unknown error")
        error = _Rendezvous(state, None, None, None)
        self.stub_mock.return_value.make.side_effect = error

        with patch("grpc.aio.insecure_channel", return_value=self.channel_mock) as channel_mock:
            with self.assertRaisesRegex(_Rendezvous, "status = StatusCode.UNKNOWN"):
                await self.client.call("make", Empty())

        self.assertEqual(channel_mock.mock_calls, [])

    async def test_call_stream(self):
        data: Iterable[tuple[tuple, dict]] = (
            # args, kwargs
            (("make", sentinel.request), {}),
            (("make",), {"request": sentinel.request}),
            ((), {"method": "make", "request": sentinel.request}),
        )
        for args, kwargs in data:
            with self.subTest(args=args, kwargs=kwargs):
                stub_mock = Mock()
                with patch("grpc.aio.insecure_channel", return_value=self.channel_mock):
                    client = AsyncGrpcClient(sentinel.netloc, stub_cls=stub_mock)
                    client.channel
                reply = Reply()
                reply.data.result = "Espresso!"
                stub_mock.return_value.make.return_value = aiter([reply])

                result = client.call_stream(*args, **kwargs)

                self.assertIsInstance(result, types.AsyncGeneratorType)
                self.assertEqual(await alist(result), ["Espresso!"])
                self.assertEqual(stub_mock.return_value.mock_calls, [call.make(sentinel.request, timeout=None)])

    async def test_call_stream_timeout(self):
        data: Iterable[tuple[dict, dict, Any]] = (
            # client_kwargs, call_kwargs, timeout
            ({}, {}, None),
            ({"timeout": sentinel.timeout}, {}, sentinel.timeout),
            ({}, {"timeout": sentinel.timeout}, sentinel.timeout),
            ({"timeout": sentinel.default}, {"timeout": sentinel.timeout}, sentinel.timeout),
        )
        for client_kwargs, call_kwargs, timeout in data:
            with self.subTest(client_kwargs=client_kwargs, call_kwargs=call_kwargs):
                stub_mock = Mock()
                with patch("grpc.aio.insecure_channel", return_value=self.channel_mock):
                    client = AsyncGrpcClient(sentinel.netloc, stub_cls=stub_mock, **client_kwargs)
                    client.channel
                reply = Reply()
                reply.data.result = "Espresso!"
                stub_mock.return_value.make.return_value = aiter([reply])

                self.assertEqual(await alist(client.call_stream("make", Empty(), **call_kwargs)), ["Espresso!"])

                self.assertEqual(stub_mock.return_value.mock_calls, [call.make(Empty(), timeout=timeout)])

    async def test_call_stream__empty_empty(self):
        self.stub_mock.return_value.make.return_value = aiter([Empty(), Empty(), Empty()])

        with patch("frgal.aio.get_call_id", return_value="XXXX"):
            self.assertEqual(await alist(self.client.call_stream("make", Empty())), [None, None, None])

        self.log_handler.check(
            ("frgal.aio", "DEBUG", "[XXXX] stream make started"),
            ("frgal.aio", "DEBUG", "[XXXX] make()"),
            ("frgal.aio", "DEBUG", "[XXXX] make returned "),
            ("frgal.aio", "DEBUG", "[XXXX] make returned "),
            ("frgal.aio", "DEBUG", "[XXXX] make returned "),
            ("frgal.aio", "DEBUG", "[XXXX] stream make returned 3 chunks"),
        )

    async def test_call_stream__unary(self):
        request = SimpleMessage(value="Answers!")
        self.stub_mock.return_value.make.return_value = aiter([make_reply("42"), make_reply("43"), make_reply("44")])

        with patch("frgal.aio.get_call_id", return_value="XXXX"):
            self.assertEqual(await alist(self.client.call_stream("make", request)), ["42", "43", "44"])

        self.log_handler.check(
            ("frgal.aio", "DEBUG", "[XXXX] stream make started"),
            ("frgal.aio", "DEBUG", '[XXXX] make(value: "Answers!")'),
            ("frgal.aio", "DEBUG", '[XXXX] make returned data { result: "42" }'),
            ("frgal.aio", "DEBUG", '[XXXX] make returned data { result: "43" }'),
            ("frgal.aio", "DEBUG", '[XXXX] make returned data { result: "44" }'),
            ("frgal.aio", "DEBUG", "[XXXX] stream make returned 3 chunks"),
        )

    async def test_call_stream__stream(self):
        requests = iter((SimpleMessage(value="Lister"), SimpleMessage(value="Rimmer"), SimpleMessage(value="Kryten")))

        # Mock method to consumer the requests
        async def _make(requests, *args, **kwargs):
            await atuple(requests)
            yield make_reply("42")
            yield make_reply("43")
            yield make_reply("44")

        self.stub_mock.return_value.make.side_effect = _make

        with patch("frgal.aio.get_call_id", return_value="XXXX"):
            self.assertEqual(await alist(self.client.call_stream("make", requests)), ["42", "43", "44"])

        self.log_handler.check(
            ("frgal.aio", "DEBUG", "[XXXX] stream make started"),
            ("frgal.aio", "DEBUG", '[XXXX] make(value: "Lister")'),
            ("frgal.aio", "DEBUG", '[XXXX] make(value: "Rimmer")'),
            ("frgal.aio", "DEBUG", '[XXXX] make(value: "Kryten")'),
            ("frgal.aio", "DEBUG", '[XXXX] make returned data { result: "42" }'),
            ("frgal.aio", "DEBUG", '[XXXX] make returned data { result: "43" }'),
            ("frgal.aio", "DEBUG", '[XXXX] make returned data { result: "44" }'),
            ("frgal.aio", "DEBUG", "[XXXX] stream make returned 3 chunks"),
        )

    async def test_call_stream__max_count(self):
        request = SimpleMessage(value="Answers!")
        self.stub_mock.return_value.make.return_value = aiter([make_reply("42"), make_reply("43"), make_reply("44")])

        with patch("frgal.aio.MESSAGE_MAX_COUNT", 2):
            with patch("frgal.aio.get_call_id", return_value="XXXX"):
                self.assertEqual(await alist(self.client.call_stream("make", request)), ["42", "43", "44"])

        self.log_handler.check(
            ("frgal.aio", "DEBUG", "[XXXX] stream make started"),
            ("frgal.aio", "DEBUG", '[XXXX] make(value: "Answers!")'),
            ("frgal.aio", "DEBUG", '[XXXX] make returned data { result: "42" }'),
            ("frgal.aio", "DEBUG", '[XXXX] make returned data { result: "43" }'),
            ("frgal.aio", "DEBUG", "[XXXX] stream make returned 3 chunks"),
        )

    async def test_call_stream_error(self):
        state = grpc._channel._RPCState((), "", "", grpc.StatusCode.UNKNOWN, "Unknown error")
        error = _Rendezvous(state, None, None, None)

        async def error_iterator():
            raise error
            yield None  # type: ignore # pragma: no cover

        self.stub_mock.return_value.make.return_value = error_iterator()

        with patch("frgal.aio.get_call_id", return_value="XXXX"):
            with self.assertRaisesRegex(_Rendezvous, "status = StatusCode.UNKNOWN"):
                async for _response in self.client.call_stream("make", Empty()):  # pragma: no cover
                    pass

        self.log_handler.check(
            ("frgal.aio", "DEBUG", "[XXXX] stream make started"),
            ("frgal.aio", "DEBUG", "[XXXX] make()"),
            (
                "frgal.aio",
                "INFO",
                "[XXXX] stream make failed after 0 chunks with <_MultiThreadedRendezvous of RPC that terminated with:\n"
                "\tstatus = StatusCode.UNKNOWN\n"
                '\tdetails = "Unknown error"\n'
                '\tdebug_error_string = "None"\n'
                ">",
            ),
        )

    def test_decoder_default(self):
        client = AsyncGrpcClient(sentinel.netloc, stub_cls=sentinel.stub)

        self.assertEqual(type(client.decoder), GrpcDecoder)

    def test_decoder_custom(self):
        client = AsyncGrpcClient(sentinel.netloc, stub_cls=sentinel.stub, decoder=sentinel.decoder)

        self.assertEqual(client.decoder, sentinel.decoder)

    def test_decoder_class(self):
        class TestDecoder(GrpcDecoder):
            pass

        class TestClient(AsyncGrpcClient):
            decoder_cls = TestDecoder

        client = TestClient(sentinel.netloc, stub_cls=sentinel.stub)

        self.assertIsInstance(client.decoder, TestDecoder)

    def test_options_default(self):
        client = AsyncGrpcClient(sentinel.netloc, stub_cls=sentinel.stub)

        default_options = {
            "grpc.min_reconnect_backoff_ms": GRPC_MIN_RECONNECT_BACKOFF_MS,
            "grpc.max_reconnect_backoff_ms": GRPC_MAX_RECONNECT_BACKOFF_MS,
        }
        self.assertEqual(client.options, default_options)

    def test_options_custom(self):
        client = AsyncGrpcClient(sentinel.netloc, stub_cls=sentinel.stub, options={"key": sentinel.value})

        options = {
            "grpc.min_reconnect_backoff_ms": GRPC_MIN_RECONNECT_BACKOFF_MS,
            "grpc.max_reconnect_backoff_ms": GRPC_MAX_RECONNECT_BACKOFF_MS,
            "key": sentinel.value,
        }
        self.assertEqual(client.options, options)

    def test_options_override(self):
        client = AsyncGrpcClient(
            sentinel.netloc, stub_cls=sentinel.stub, options={"grpc.min_reconnect_backoff_ms": sentinel.value}
        )

        options = {
            "grpc.min_reconnect_backoff_ms": sentinel.value,
            "grpc.max_reconnect_backoff_ms": GRPC_MAX_RECONNECT_BACKOFF_MS,
        }
        self.assertEqual(client.options, options)

    def test_options_class(self):
        class TestClient(AsyncGrpcClient):
            options = {"key": sentinel.value}

        client = TestClient(sentinel.netloc, stub_cls=sentinel.stub)

        self.assertEqual(client.options, {"key": sentinel.value})

    def test_stub_cls(self):
        class TestClient(AsyncGrpcClient):
            stub_cls = sentinel.cls_stub

        data: tuple[tuple[Callable, Any], ...] = (
            # client_cls, stub_cls
            (partial(AsyncGrpcClient, stub_cls=sentinel.stub_cls), sentinel.stub_cls),
            # Client with class default
            (TestClient, sentinel.cls_stub),
            # Override class defaults by instance defaults.
            (partial(TestClient, stub_cls=sentinel.stub_cls), sentinel.stub_cls),
            (partial(TestClient, stub_cls=None), sentinel.cls_stub),
        )
        for client_cls, stub_cls in data:
            with self.subTest(client_cls=client_cls):
                client = client_cls(sentinel.netloc)

                self.assertEqual(client.stub_cls, stub_cls)

    def test_timeout(self):
        class TestClient(AsyncGrpcClient):
            timeout = sentinel.cls_timeout

        data: tuple[tuple[Callable, Any], ...] = (
            # client_cls, timeout
            (AsyncGrpcClient, None),
            (partial(AsyncGrpcClient, timeout=sentinel.timeout), sentinel.timeout),
            # Client with class default
            (TestClient, sentinel.cls_timeout),
            # Override class defaults by instance defaults.
            (partial(TestClient, timeout=sentinel.timeout), sentinel.timeout),
            (partial(TestClient, timeout=None), None),
        )
        for client_cls, timeout in data:
            with self.subTest(client_cls=client_cls):
                client = client_cls(sentinel.netloc, stub_cls=sentinel.stub)

                self.assertEqual(client.timeout, timeout)

    async def test_semaphore(self):
        # We create pairs of run events and stop events.
        # We use asyncio.gather to run N calls + 1 control task.
        # Control task waits sets stop events sequentially and checks
        # number of newly started after each.
        start_events = [asyncio.Event() for _ in range(5)]
        stop_events = [asyncio.Event() for _ in range(5)]
        current_task = [0]

        semaphore = asyncio.Semaphore(1)

        async def make(request, **kwargs):
            start_events[current_task[-1]].set()
            await stop_events[current_task[-1]].wait()
            return Empty()

        async def make_stream(request, **kwargs):
            start_events[current_task[-1]].set()
            await stop_events[current_task[-1]].wait()
            yield Empty()

        async def control():
            for i in range(5):
                await start_events[i].wait()
                self.assertEqual(sum([e.is_set() for e in start_events]), i + 1)
                self.assertEqual(sum([e.is_set() for e in stop_events]), i)
                stop_events[i].set()
                current_task.append(i + 1)

        with patch("grpc.aio.insecure_channel", return_value=self.channel_mock):
            client = AsyncGrpcClient(sentinel.netloc, stub_cls=self.stub_mock, semaphore=semaphore)
            client.channel

        self.stub_mock.return_value.make.side_effect = make
        self.stub_mock.return_value.make_stream.side_effect = make_stream

        with patch("frgal.aio.get_call_id", side_effect=range(5)):
            await asyncio.gather(
                control(),
                client.call("make", Empty()),
                alist(client.call_stream("make_stream", Empty())),
                client.call("make", Empty()),
                alist(client.call_stream("make_stream", Empty())),
                client.call("make", Empty()),
            )

        self.assertEqual(
            [
                log[-1]
                for log in self.log_handler.actual()
                if "make()" not in log[-1] and "make_stream()" not in log[-1]
            ],
            [
                "[0] make started",
                "[1] stream make_stream started",
                "[2] make started",
                "[3] stream make_stream started",
                "[4] make started",
                "[0] make returned ",
                "[1] make_stream returned ",
                "[1] stream make_stream returned 1 chunks",
                "[2] make returned ",
                "[3] make_stream returned ",
                "[3] stream make_stream returned 1 chunks",
                "[4] make returned ",
            ],
        )


class TestAsyncClient(AsyncTestClientMixin, AsyncGrpcClient):  # type: ignore[misc]
    def get_coffee_sync(self) -> str:
        return "Coffee"

    async def get_coffee_async(self) -> str:
        return "AsyncCoffee"

    def get_coffee_sync_gen(self, n: int = 1) -> Iterable[str]:
        for _ in range(n):
            yield "Coffee"

    async def get_coffee_async_gen(self, n: int = 1) -> AsyncIterable[str]:
        for _ in range(n):
            yield "AsyncCoffee"


class SyncGrpcProxyTest(TestCase):
    def setUp(self):
        self.log_handler = LogCapture("frgal.aio")
        self.addCleanup(self.log_handler.uninstall)

    def test_proxy_client(self):
        self.client = SyncGrpcProxy(TestAsyncClient(sentinel.netloc, stub_cls=sentinel.stub))
        self.assertEqual(self.client.netloc, sentinel.netloc)
        self.assertEqual(self.client.get_coffee_sync(), "Coffee")
        self.assertEqual(self.client.get_coffee_async(), "AsyncCoffee")
        self.assertEqual(list(self.client.get_coffee_sync_gen(n=2)), ["Coffee", "Coffee"])
        self.assertEqual(list(self.client.get_coffee_async_gen(n=2)), ["AsyncCoffee", "AsyncCoffee"])
        with self.assertRaises(AttributeError):
            self.client.not_attr

    def test_proxy_client_cls(self):
        class SyncProxy(SyncGrpcProxy):
            client_cls = TestAsyncClient

        self.client = SyncProxy(sentinel.netloc, stub_cls=sentinel.stub)
        self.assertEqual(self.client.netloc, sentinel.netloc)
        self.assertEqual(self.client.get_coffee_sync(), "Coffee")
        self.assertEqual(self.client.get_coffee_async(), "AsyncCoffee")
        self.assertEqual(list(self.client.get_coffee_sync_gen()), ["Coffee"])
        self.assertEqual(list(self.client.get_coffee_async_gen()), ["AsyncCoffee"])
        with self.assertRaises(AttributeError):
            self.client.not_attr
