import asyncio
from collections.abc import AsyncIterator
from functools import partial
from typing import Any, cast
from unittest import IsolatedAsyncioTestCase, TestCase
from unittest.mock import AsyncMock, Mock, call, sentinel

from asyncstdlib import iter as aiter, tuple as atuple
from testfixtures import ShouldNotWarn, ShouldWarn

from frgal.aio import AsyncGrpcClient
from frgal.client import GrpcClient
from frgal.utils import (
    AsyncMockingChannel,
    AsyncTestClientMixin,
    CallableMock,
    MockingChannel,
    TestClientMixin,
    get_mock_awaits,
    make_awaitable,
)

from .proto.example_pb2 import Reply, SimpleMessage  # type: ignore[attr-defined]
from .utils import make_reply


class CallableMockTest(IsolatedAsyncioTestCase):
    def test_call(self):
        a_callable = Mock(return_value=sentinel.reply)
        a_call = CallableMock("/Coffee/make", a_callable, exhaust_iterables=False)

        self.assertEqual(a_call(sentinel.request), sentinel.reply)

        self.assertEqual(a_callable.mock_calls, [call(sentinel.request)])

    async def test_exhaust(self):
        a_mock = Mock()
        a_call = CallableMock("/Coffee/make", a_mock, exhaust_iterables=True)
        data = (
            (123, 123),
            ("string", "string"),
            ((1, 2, 3), (1, 2, 3)),
            ([1, 2, 3], [1, 2, 3]),
            (iter([1, 2, 3]), (1, 2, 3)),
            (aiter([1, 2, 3]), (1, 2, 3)),
        )
        for ain, aout in data:
            with self.subTest(ain=ain, aout=aout):
                a_mock.return_value = make_awaitable(None)
                await a_call(ain)
                self.assertEqual(a_mock.mock_calls[-1], call(aout))

    async def test_not_exhaust(self):
        a_mock = Mock()
        a_call = CallableMock("/Coffee/make", a_mock, exhaust_iterables=False)
        data = ((1, 2, 3), iter([1, 2, 3]), aiter([1, 2, 3]))
        for ain in data:
            with self.subTest(ain=ain):
                a_mock.return_value = make_awaitable(None)
                await a_call(ain)
                self.assertIs(a_mock.mock_calls[-1][1][0], ain)

    async def test_exhaust_not_set_static(self):
        a_mock = Mock()
        a_call = CallableMock("/Coffee/make", a_mock)
        data = (sentinel.request, (1, 2, 3))
        for ain in data:
            with self.subTest(ain=ain):
                a_mock.return_value = make_awaitable(None)
                with ShouldNotWarn():
                    await a_call(ain)
                self.assertIs(a_mock.mock_calls[-1][1][0], ain)

    async def test_exhaust_not_set_iterable(self):
        a_mock = Mock()
        a_call = CallableMock("/Coffee/make", a_mock)
        data = (iter([1, 2, 3]), aiter([1, 2, 3]))
        for ain in data:
            with self.subTest(ain=ain):
                a_mock.return_value = make_awaitable(None)
                with ShouldWarn(
                    DeprecationWarning(
                        "Default value of `exhaust_iterables` will change to True in next major release."
                        "If you want to maintain the current behaviour, set it to False explicitely."
                    )
                ):
                    await a_call(ain)
                self.assertIs(a_mock.mock_calls[-1][1][0], ain)

    async def test_exhaust_exception(self):
        async def exception_requests():
            raise RuntimeError
            yield None  # type: ignore # pragma: no cover

        a_mock = Mock()
        a_call = CallableMock("/Coffee/make", a_mock, exhaust_iterables=True)

        with self.assertRaises(RuntimeError):
            await a_call(exception_requests())
        a_mock.assert_not_called()


class MockingChannelTest(TestCase):
    def _test_callable(self, name: str) -> None:
        client = Mock(name="client")
        channel = MockingChannel(client)

        a_callable = getattr(channel, name)("/Coffee/make")

        a_call = cast(partial, a_callable.call)
        self.assertEqual(a_call.func, client.mock)
        self.assertEqual(a_call.args, ())
        self.assertEqual(a_call.keywords, {"method": "/Coffee/make"})

    def test_unary_unary(self):
        self._test_callable(name="unary_unary")

    def test_unary_stream(self):
        self._test_callable(name="unary_stream")

    def test_stream_unary(self):
        self._test_callable(name="stream_unary")

    def test_stream_stream(self):
        self._test_callable(name="stream_stream")


class CrewStub:
    """A simple stub for testing."""

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.channel = channel
        self.drink = channel.unary_unary("/Crew/drink")
        self.plunder = channel.unary_stream("/Crew/plunder")
        self.search = channel.stream_unary("/Crew/search")
        self.retreat = channel.stream_stream("/Crew/retreat")


class TestClient(TestClientMixin, GrpcClient):  # type: ignore[misc]
    """Simple test client to test a TestClientMixin."""

    exhaust_iterables = False


class TestClientMixinTest(TestCase):
    def test_call(self):
        client = TestClient(sentinel.netloc, stub_cls=CrewStub)
        request = SimpleMessage(value="10")
        reply = Reply()
        reply.data.result = "Espresso!"
        client.mock.return_value = reply

        result = client.call("drink", request)

        self.assertEqual(result, "Espresso!")
        self.assertEqual(client.mock.mock_calls, [call(request, method="/Crew/drink", timeout=None)])

    def test_call_future(self):
        client = TestClient(sentinel.netloc, stub_cls=CrewStub)
        request = SimpleMessage(value="10")
        reply = Reply()
        reply.data.result = "Espresso!"
        client.mock.return_value = reply

        with self.assertWarnsRegex(
            DeprecationWarning, "Method call_future is deprecated and will be removed. Use async instead."
        ):
            result = client.call_future("drink", request)

        self.assertEqual(result.result(), "Espresso!")
        self.assertEqual(client.mock.mock_calls, [call(request, method="/Crew/drink", timeout=None)])

    def test_call_stream(self):
        client = TestClient(sentinel.netloc, stub_cls=CrewStub)
        request = SimpleMessage(value="10")
        reply = Reply()
        reply.data.result = "Espresso!"
        client.mock.return_value = [reply, reply]

        result = client.call_stream("plunder", request)

        self.assertEqual(tuple(result), ("Espresso!", "Espresso!"))
        self.assertEqual(client.mock.mock_calls, [call(request, method="/Crew/plunder", timeout=None)])


class AsyncTestClient(AsyncTestClientMixin, AsyncGrpcClient):  # type: ignore[misc]
    """Simple test client to test a AsyncTestClientMixin."""

    exhaust_iterables = True

    async def drink(self, request: Any) -> Any:
        return await self.call("drink", request)

    async def plunder(self, request: Any) -> AsyncIterator[Any]:
        async for i in self.call_stream("plunder", request):
            yield i

    async def search(self, requests: Any) -> Any:
        return await self.call("search", requests)

    async def retreat(self, requests: Any) -> AsyncIterator[Any]:
        async for i in self.call_stream("retreat", requests):
            yield i


class AsyncTestClientMixinTest(IsolatedAsyncioTestCase):
    def test_init(self):
        client = AsyncTestClient(sentinel.netloc, stub_cls=CrewStub)
        self.assertIsInstance(client.mock, AsyncMock)
        self.assertIsInstance(client.channel, AsyncMockingChannel)
        self.assertIsInstance(client.channel._loop, asyncio.BaseEventLoop)

    async def test_unary_unary(self):
        client = AsyncTestClient(sentinel.netloc, stub_cls=CrewStub)
        client.mock.return_value = make_reply("beer")

        self.assertEqual(await client.drink(sentinel.lister), "beer")

        self.assertEqual(get_mock_awaits(client.mock), [call(sentinel.lister, method="/Crew/drink", timeout=None)])
        self.assertEqual(client.stream_mock.mock_calls, [])

    async def test_unary_stream(self):
        client = AsyncTestClient(sentinel.netloc, stub_cls=CrewStub)
        client.stream_mock.return_value = aiter((make_reply("beer"), make_reply("naan"), make_reply("curry")))

        self.assertEqual(await atuple(client.plunder(sentinel.ship)), ("beer", "naan", "curry"))

        self.assertEqual(get_mock_awaits(client.mock), [])
        self.assertEqual(
            client.stream_mock.mock_calls,
            [call(sentinel.ship, method="/Crew/plunder", timeout=None)],
        )

    async def test_stream_unary(self):
        client = AsyncTestClient(sentinel.netloc, stub_cls=CrewStub)
        client.mock.return_value = make_reply("troubles")

        self.assertEqual(await client.search((sentinel.lister, sentinel.rimmer, sentinel.kryten)), "troubles")

        self.assertEqual(
            get_mock_awaits(client.mock),
            [call((sentinel.lister, sentinel.rimmer, sentinel.kryten), method="/Crew/search", timeout=None)],
        )
        self.assertEqual(client.stream_mock.mock_calls, [])

    async def test_stream_unary_aiter(self):
        client = AsyncTestClient(sentinel.netloc, stub_cls=CrewStub)
        client.mock.return_value = make_reply("troubles")

        self.assertEqual(await client.search(aiter((sentinel.lister, sentinel.rimmer, sentinel.kryten))), "troubles")

        self.assertEqual(
            get_mock_awaits(client.mock),
            [call((sentinel.lister, sentinel.rimmer, sentinel.kryten), method="/Crew/search", timeout=None)],
        )
        self.assertEqual(client.stream_mock.mock_calls, [])

    async def test_stream_stream(self):
        client = AsyncTestClient(sentinel.netloc, stub_cls=CrewStub)
        client.stream_mock.return_value = aiter((make_reply("lister"), make_reply("rimmer"), make_reply("cat")))

        self.assertEqual(
            await atuple(client.retreat((sentinel.simulant, sentinel.inquisitor, sentinel.psirens))),
            ("lister", "rimmer", "cat"),
        )

        self.assertEqual(get_mock_awaits(client.mock), [])
        self.assertEqual(
            client.stream_mock.mock_calls,
            [call((sentinel.simulant, sentinel.inquisitor, sentinel.psirens), method="/Crew/retreat", timeout=None)],
        )


class GetMockAwaitsTest(IsolatedAsyncioTestCase):
    async def test_await(self):
        mock = AsyncMock()
        await mock(sentinel.arg)

        self.assertEqual(get_mock_awaits(mock), [call(sentinel.arg)])

    async def test_await_method(self):
        mock = AsyncMock()
        await mock.foo(sentinel.arg)

        self.assertEqual(get_mock_awaits(mock), [call.foo(sentinel.arg)])

    async def test_await_children(self):
        mock = AsyncMock()
        await mock.sub.foo(sentinel.arg)

        self.assertEqual(get_mock_awaits(mock), [call.sub.foo(sentinel.arg)])

    async def test_nested_awaits(self):
        mock = AsyncMock()
        result = await mock.sub(sentinel.arg)
        await result.foo(sentinel.other)

        self.assertEqual(get_mock_awaits(mock), [call.sub(sentinel.arg), call.sub().foo(sentinel.other)])

    async def test_no_await(self):
        """Test coroutine creation itself is not listed."""
        mock = AsyncMock()
        coro = mock.foo()

        self.assertEqual(get_mock_awaits(mock), [])
        # Await coroutine to prevent RuntimeWarning
        await coro

    async def test_multiple_awaits(self):
        mock = AsyncMock()
        await mock(sentinel.arg)
        await mock(sentinel.arg)
        await mock(sentinel.another)

        self.assertEqual(get_mock_awaits(mock), [call(sentinel.arg), call(sentinel.arg), call(sentinel.another)])
