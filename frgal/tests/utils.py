from .proto.example_pb2 import Reply  # type: ignore[attr-defined]


def make_reply(result: str) -> Reply:
    """Return simple Reply."""
    reply = Reply()
    reply.data.result = result
    return reply
