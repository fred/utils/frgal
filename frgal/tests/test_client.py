"""Tests for client module."""

import types
from collections.abc import Iterable
from functools import partial
from typing import Any, Callable
from unittest import TestCase
from unittest.mock import Mock, call, patch, sentinel

import grpc
import grpc._channel
from google.protobuf.empty_pb2 import Empty
from grpc import Channel
from testfixtures import LogCapture, SequenceComparison, TempDirectory

from frgal.client import CallWrapper, FutureCallWrapper, GrpcClient, StreamCallWrapper, make_credentials
from frgal.constants import GRPC_MAX_RECONNECT_BACKOFF_MS, GRPC_MIN_RECONNECT_BACKOFF_MS
from frgal.decoder import GrpcDecoder
from frgal.exceptions import UnknownError

from .proto.example_pb2 import Reply, SimpleMessage  # type: ignore[attr-defined]
from .utils import make_reply

try:
    from grpc._channel import _MultiThreadedRendezvous as _Rendezvous
except ImportError:  # pragma: no cover
    from grpc._channel import _Rendezvous


class CallWrapperTest(TestCase):
    def setUp(self):
        self.client = Mock(name="client")
        self.client.decoder = GrpcDecoder()

        self.request = SimpleMessage(value="One espresso, please!")

        self.log_handler = LogCapture()
        self.addCleanup(self.log_handler.uninstall)

    def test_get_reply(self):
        a_call = Mock(name="call", return_value=sentinel.reply)
        a_call._method = b"make"
        wrapper = CallWrapper(a_call, sentinel.request, client=sentinel.client)

        self.assertEqual(wrapper.get_reply(), sentinel.reply)

        self.assertEqual(a_call.mock_calls, [call(sentinel.request, timeout=None)])

    def test_get_reply_timeout(self):
        a_call = Mock(name="call", return_value=sentinel.reply)
        a_call._method = b"make"
        wrapper = CallWrapper(a_call, sentinel.request, client=sentinel.client, timeout=sentinel.timeout)

        self.assertEqual(wrapper.get_reply(), sentinel.reply)

        self.assertEqual(a_call.mock_calls, [call(sentinel.request, timeout=sentinel.timeout)])

    def test_decode_reply(self):
        a_call = Mock(name="call")
        a_call._method = b"make"
        wrapper = CallWrapper(a_call, sentinel.request, client=self.client)
        reply = Reply()
        reply.data.result = "Espresso!"

        self.assertEqual(wrapper.decode_reply(reply), "Espresso!")

    def test_decode_reply_empty(self):
        a_call = Mock(name="call")
        a_call._method = b"make"
        wrapper = CallWrapper(a_call, sentinel.request, client=self.client)

        self.assertIsNone(wrapper.decode_reply(Empty()))

    def test_decode_reply_error(self):
        a_call = Mock(name="call")
        a_call._method = b"make"
        wrapper = CallWrapper(a_call, sentinel.request, client=self.client)
        reply = Reply()
        reply.exception.tea_pot_error.CopyFrom(Reply.Exception.TeaPotError())

        with self.assertRaisesRegex(UnknownError, "tea_pot_error"):
            wrapper.decode_reply(reply)

    def test_decode_reply_modify_result(self):
        a_call = Mock(name="call")
        a_call._method = b"make"
        modifier = Mock(name="modifier", return_value=sentinel.modified)
        wrapper = CallWrapper(a_call, sentinel.request, client=self.client, modify_result=modifier)
        reply = Reply()
        reply.data.result = "Espresso!"

        self.assertEqual(wrapper.decode_reply(reply), sentinel.modified)
        self.assertEqual(modifier.mock_calls, [call("Espresso!")])

    def test_result(self):
        reply = Reply()
        reply.data.result = "Espresso!"
        a_call = Mock(name="call", return_value=reply)
        a_call._method = b"make"
        with patch("frgal.client.get_call_id", return_value="XXXX"):
            wrapper = CallWrapper(a_call, self.request, client=self.client)

        self.assertEqual(wrapper.result(), "Espresso!")

        self.assertEqual(a_call.mock_calls, [call(self.request, timeout=None)])

        self.log_handler.check(
            ("frgal.client", "DEBUG", "[XXXX] make started"),
            ("frgal.client", "DEBUG", '[XXXX] make(value: "One espresso, please!")'),
            ("frgal.client", "DEBUG", '[XXXX] make returned data { result: "Espresso!" }'),
        )

    def test_result_grpc_error(self):
        state = grpc._channel._RPCState((), "", "", grpc.StatusCode.UNKNOWN, "Unknown error")
        error = _Rendezvous(state, None, None, None)
        a_call = Mock(name="call", side_effect=error)
        a_call._method = b"make"
        with patch("frgal.client.get_call_id", return_value="XXXX"):
            wrapper = CallWrapper(a_call, self.request, client=self.client)

        with self.assertRaisesRegex(grpc.RpcError, "status = StatusCode.UNKNOWN"):
            wrapper.result()

        self.assertEqual(a_call.mock_calls, [call(self.request, timeout=None)])

        self.log_handler.check(
            ("frgal.client", "DEBUG", "[XXXX] make started"),
            ("frgal.client", "DEBUG", '[XXXX] make(value: "One espresso, please!")'),
            (
                "frgal.client",
                "INFO",
                "[XXXX] make failed with <_MultiThreadedRendezvous of RPC that terminated with:\n"
                "\tstatus = StatusCode.UNKNOWN\n"
                '\tdetails = "Unknown error"\n'
                '\tdebug_error_string = "None"\n'
                ">",
            ),
        )

    def test_result_retry(self):
        state = grpc._channel._RPCState((), "", "", grpc.StatusCode.UNAVAILABLE, "Connection reset by peer")
        error = _Rendezvous(state, None, None, None)
        reply = Reply()
        reply.data.result = "Espresso!"
        a_call = Mock(name="call", side_effect=[error, reply])
        a_call._method = b"make"
        with patch("frgal.client.get_call_id", return_value="XXXX"):
            wrapper = CallWrapper(a_call, self.request, client=self.client)

        self.assertEqual(wrapper.result(), "Espresso!")

        self.assertEqual(a_call.mock_calls, [call(self.request, timeout=None), call(self.request, timeout=None)])
        self.assertEqual(self.client.mock_calls, [])

        self.log_handler.check(
            ("frgal.client", "DEBUG", "[XXXX] make started"),
            ("frgal.client", "DEBUG", '[XXXX] make(value: "One espresso, please!")'),
            ("frgal.client", "INFO", "[XXXX] Connection failed. Attempting retry."),
            ("frgal.client", "DEBUG", '[XXXX] make(value: "One espresso, please!")'),
            ("frgal.client", "DEBUG", '[XXXX] make returned data { result: "Espresso!" }'),
        )

    def test_result_retry_failed(self):
        state = grpc._channel._RPCState((), "", "", grpc.StatusCode.UNAVAILABLE, "Connection reset by peer")
        error = _Rendezvous(state, None, None, None)
        reply = Reply()
        reply.data.result = "Espresso!"
        a_call = Mock(name="call", side_effect=error)
        a_call._method = b"make"
        with patch("frgal.client.get_call_id", return_value="XXXX"):
            wrapper = CallWrapper(a_call, self.request, client=self.client, max_retries=2)

        with self.assertRaisesRegex(grpc.RpcError, "status = StatusCode.UNAVAILABLE"):
            wrapper.result()

        self.assertEqual(a_call.mock_calls, [call(self.request, timeout=None), call(self.request, timeout=None)])

        self.log_handler.check(
            ("frgal.client", "DEBUG", "[XXXX] make started"),
            ("frgal.client", "DEBUG", '[XXXX] make(value: "One espresso, please!")'),
            ("frgal.client", "INFO", "[XXXX] Connection failed. Attempting retry."),
            ("frgal.client", "DEBUG", '[XXXX] make(value: "One espresso, please!")'),
            ("frgal.client", "INFO", "[XXXX] Connection failed. Retries were unsuccessful."),
            (
                "frgal.client",
                "INFO",
                "[XXXX] make failed with <_MultiThreadedRendezvous of RPC that terminated with:\n"
                "\tstatus = StatusCode.UNAVAILABLE\n"
                '\tdetails = "Connection reset by peer"\n'
                '\tdebug_error_string = "None"\n'
                ">",
            ),
        )


class TestFutureCallWrapper(TestCase):
    def setUp(self):
        self.client = Mock(name="client")
        self.client.decoder = GrpcDecoder()

        self.request = SimpleMessage(value="One espresso, please!")

        self.log_handler = LogCapture()
        self.addCleanup(self.log_handler.uninstall)

    def test_init(self):
        a_call = Mock(name="call")
        a_call.future.return_value = sentinel.future

        FutureCallWrapper(a_call, sentinel.request, client=self.client)

        # Check the future is called immediately.
        self.assertEqual(a_call.mock_calls, [call.future(sentinel.request, timeout=None)])

    def test_init_timeout(self):
        a_call = Mock(name="call")
        a_call.future.return_value = sentinel.future

        FutureCallWrapper(a_call, sentinel.request, client=self.client, timeout=sentinel.timeout)

        # Check the future is called immediately.
        self.assertEqual(a_call.mock_calls, [call.future(sentinel.request, timeout=sentinel.timeout)])

    def test_get_reply(self):
        a_call = Mock(name="call")
        a_call.future.return_value.result.return_value = sentinel.reply
        wrapper = FutureCallWrapper(a_call, sentinel.request, client=self.client)

        self.assertEqual(wrapper.get_reply(), sentinel.reply)

        self.assertEqual(a_call.mock_calls, [call.future(sentinel.request, timeout=None), call.future().result()])
        self.assertEqual(self.client.mock_calls, [])


class StreamCallWrapperTest(TestCase):
    def setUp(self):
        self.client = Mock(name="client")
        self.client.decoder = GrpcDecoder()

        self.log_handler = LogCapture()
        self.addCleanup(self.log_handler.uninstall)

    def test_decode_reply(self):
        a_call = Mock(name="call")
        a_call._method = b"make"
        wrapper = StreamCallWrapper(a_call, sentinel.request, client=self.client)
        reply = Reply()
        reply.data.result = "Espresso!"

        result = wrapper.decode_reply([reply])

        self.assertIsInstance(result, types.GeneratorType)
        self.assertEqual(tuple(result), ("Espresso!",))

    def test_decode_reply_modify_result(self):
        a_call = Mock(name="call")
        a_call._method = b"make"
        modifier = Mock(name="modifier", return_value=sentinel.modified)
        wrapper = StreamCallWrapper(a_call, sentinel.request, client=self.client, modify_result=modifier)
        reply = Reply()
        reply.data.result = "Espresso!"

        result = wrapper.decode_reply([reply])

        self.assertIsInstance(result, types.GeneratorType)
        self.assertEqual(tuple(result), (sentinel.modified,))
        self.assertEqual(modifier.mock_calls, [call("Espresso!")])


class TestGrpcClient(TestCase):
    """Test GrpcClient."""

    default_options = (
        ("grpc.min_reconnect_backoff_ms", GRPC_MIN_RECONNECT_BACKOFF_MS),
        ("grpc.max_reconnect_backoff_ms", GRPC_MAX_RECONNECT_BACKOFF_MS),
    )

    def setUp(self):
        self.stub_mock = Mock()
        self.stub_mock.return_value.make._method = b"make"
        self.channel_mock = Mock(spec=Channel)

        with patch("frgal.client.grpc.insecure_channel", return_value=self.channel_mock):
            self.client = GrpcClient(sentinel.netloc, stub_cls=self.stub_mock)
            self.client.channel

        self.log_handler = LogCapture()
        self.addCleanup(self.log_handler.uninstall)

    def test_init_no_stub_cls(self):
        with self.assertRaisesRegex(ValueError, "No stub_cls provided."):
            GrpcClient(sentinel.netloc)

    def test_channel_insecure(self):
        client = GrpcClient(sentinel.netloc, stub_cls=sentinel.stub)
        with patch("frgal.client.grpc.insecure_channel", return_value=self.channel_mock) as mock:
            client.channel

        self.assertEqual(client.channel, self.channel_mock)
        self.assertEqual(mock.mock_calls, [call(sentinel.netloc, SequenceComparison(*self.default_options))])

    def test_channel_insecure_options(self):
        client = GrpcClient(sentinel.netloc, stub_cls=sentinel.stub, options={"key": sentinel.value})
        with patch("frgal.client.grpc.insecure_channel", return_value=self.channel_mock) as mock:
            client.channel

        self.assertEqual(client.channel, self.channel_mock)
        options = dict(self.default_options, key=sentinel.value)
        self.assertEqual(mock.mock_calls, [call(sentinel.netloc, SequenceComparison(*options.items()))])

    def test_channel_secure(self):
        client = GrpcClient(sentinel.netloc, stub_cls=sentinel.stub, credentials=sentinel.credentials)
        with patch("frgal.client.grpc.secure_channel", return_value=self.channel_mock) as mock:
            client.channel

        self.assertEqual(client.channel, self.channel_mock)
        self.assertEqual(
            mock.mock_calls, [call(sentinel.netloc, sentinel.credentials, SequenceComparison(*self.default_options))]
        )

    def test_channel_secure_options(self):
        client = GrpcClient(
            sentinel.netloc, stub_cls=sentinel.stub, credentials=sentinel.credentials, options={"key": sentinel.value}
        )
        with patch("frgal.client.grpc.secure_channel", return_value=self.channel_mock) as mock:
            client.channel

        self.assertEqual(client.channel, self.channel_mock)
        options = dict(self.default_options, key=sentinel.value)
        self.assertEqual(
            mock.mock_calls, [call(sentinel.netloc, sentinel.credentials, SequenceComparison(*options.items()))]
        )

    def test_ssl_cert(self):
        with patch("frgal.client.make_credentials", return_value=sentinel.credentials) as mock_credentials:
            client = GrpcClient(sentinel.netloc, stub_cls=sentinel.stub, ssl_cert=sentinel.ssl_cert)
        with patch("frgal.client.grpc.secure_channel", return_value=self.channel_mock) as mock_channel:
            client.channel

        self.assertEqual(client.channel, self.channel_mock)
        self.assertEqual(mock_credentials.mock_calls, [call(sentinel.ssl_cert)])
        self.assertEqual(
            mock_channel.mock_calls,
            [call(sentinel.netloc, sentinel.credentials, SequenceComparison(*self.default_options))],
        )

    def test_credentials_ssl_cert_conflict(self):
        with self.assertRaisesRegex(ValueError, "Only one of credentials and ssl_cert may be provided"):
            GrpcClient(  # type: ignore[call-overload]
                sentinel.netloc,
                stub_cls=sentinel.stub,
                ssl_cert=sentinel.ssl_cert,
                credentials=sentinel.credentials,
            )

    def test_get_stub(self):
        stub_mock = Mock(return_value=sentinel.coffee_stub)

        client = GrpcClient(sentinel.netloc, stub_cls=stub_mock)

        with patch("frgal.client.grpc.insecure_channel", return_value=sentinel.channel):
            stub = client.get_stub()

        self.assertEqual(stub, sentinel.coffee_stub)
        self.assertEqual(stub_mock.mock_calls, [call(sentinel.channel)])

    def test_get_stub_cache(self):
        # Test stubs are cached
        stub_mock = Mock(return_value=sentinel.coffee_stub)

        client = GrpcClient(sentinel.netloc, stub_cls=stub_mock)

        # Get stub once
        with patch("frgal.client.grpc.insecure_channel", return_value=sentinel.channel):
            client.get_stub()
        # Get stub twice
        self.assertEqual(client.get_stub(), sentinel.coffee_stub)
        # Check it was created only once
        self.assertEqual(stub_mock.mock_calls, [call(sentinel.channel)])

    def test_call(self):
        data: Iterable[tuple[tuple, dict]] = (
            # args, kwargs
            (("make", sentinel.request), {}),
            (("make",), {"request": sentinel.request}),
            ((), {"method": "make", "request": sentinel.request}),
        )
        for args, kwargs in data:
            with self.subTest(args=args, kwargs=kwargs):
                stub_mock = Mock()
                client = GrpcClient(sentinel.netloc, stub_cls=stub_mock)
                reply = Reply()
                reply.data.result = "Espresso!"
                stub_mock.return_value.make.return_value = reply
                stub_mock.return_value.make._method = b"make"

                with patch("frgal.client.grpc.insecure_channel", return_value=self.channel_mock):
                    self.assertEqual(client.call(*args, **kwargs), "Espresso!")

                self.assertEqual(stub_mock.return_value.mock_calls, [call.make(sentinel.request, timeout=None)])

    def test_call_timeout(self):
        data: Iterable[tuple[dict, dict, Any]] = (
            # client_kwargs, call_kwargs, timeout
            ({}, {}, None),
            ({"timeout": sentinel.timeout}, {}, sentinel.timeout),
            ({}, {"timeout": sentinel.timeout}, sentinel.timeout),
            ({"timeout": sentinel.default}, {"timeout": sentinel.timeout}, sentinel.timeout),
        )
        for client_kwargs, call_kwargs, timeout in data:
            with self.subTest(client_kwargs=client_kwargs, call_kwargs=call_kwargs):
                stub_mock = Mock()
                client = GrpcClient(sentinel.netloc, stub_cls=stub_mock, **client_kwargs)
                reply = Reply()
                reply.data.result = "Espresso!"
                stub_mock.return_value.make.return_value = reply
                stub_mock.return_value.make._method = b"make"

                with patch("frgal.client.grpc.insecure_channel", return_value=self.channel_mock):
                    self.assertEqual(client.call("make", Empty(), **call_kwargs), "Espresso!")

                self.assertEqual(stub_mock.return_value.mock_calls, [call.make(Empty(), timeout=timeout)])

    def test_call__empty_empty(self):
        self.stub_mock.return_value.make.return_value = Empty()

        with patch("frgal.client.get_call_id", return_value="XXXX"):
            self.assertEqual(self.client.call("make", Empty()), None)

        self.log_handler.check(
            ("frgal.client", "DEBUG", "[XXXX] make started"),
            ("frgal.client", "DEBUG", "[XXXX] make()"),
            ("frgal.client", "DEBUG", "[XXXX] make returned "),
        )

    def test_call__unary_unary(self):
        request = SimpleMessage(value="Answer!")
        self.stub_mock.return_value.make.return_value = make_reply("42")

        with patch("frgal.client.get_call_id", return_value="XXXX"):
            self.assertEqual(self.client.call("make", request), "42")

        self.log_handler.check(
            ("frgal.client", "DEBUG", "[XXXX] make started"),
            ("frgal.client", "DEBUG", '[XXXX] make(value: "Answer!")'),
            ("frgal.client", "DEBUG", '[XXXX] make returned data { result: "42" }'),
        )

    def test_call__stream_unary(self):
        requests = iter((SimpleMessage(value="Lister"), SimpleMessage(value="Rimmer"), SimpleMessage(value="Kryten")))

        # Mock method to consumer the requests
        def _make(requests, *args, **kwargs):
            tuple(requests)
            return make_reply("Red Dwarf")

        self.stub_mock.return_value.make.side_effect = _make

        with patch("frgal.client.get_call_id", return_value="XXXX"):
            self.assertEqual(self.client.call("make", requests), "Red Dwarf")

        self.log_handler.check(
            ("frgal.client", "DEBUG", "[XXXX] make started"),
            ("frgal.client", "DEBUG", '[XXXX] make(value: "Lister")'),
            ("frgal.client", "DEBUG", '[XXXX] make(value: "Rimmer")'),
            ("frgal.client", "DEBUG", '[XXXX] make(value: "Kryten")'),
            ("frgal.client", "DEBUG", '[XXXX] make returned data { result: "Red Dwarf" }'),
        )

    def test_call_error(self):
        client = GrpcClient(sentinel.netloc, stub_cls=self.stub_mock)
        reply = Reply()
        reply.exception.tea_pot_error.CopyFrom(Reply.Exception.TeaPotError())
        self.stub_mock.return_value.make.return_value = reply

        with patch("frgal.client.grpc.insecure_channel", return_value=self.channel_mock):
            with self.assertRaisesRegex(UnknownError, "tea_pot_error"):
                client.call("make", Empty())

    def test_call_grpc_unavailable(self):
        client = GrpcClient(sentinel.netloc, stub_cls=self.stub_mock)
        state = grpc._channel._RPCState((), "", "", grpc.StatusCode.UNAVAILABLE, "Connection reset by peer")
        error = _Rendezvous(state, None, None, None)
        reply = Reply()
        reply.data.result = "Espresso!"
        self.stub_mock.return_value.make.side_effect = [error, error, error, reply]

        with patch("frgal.client.grpc.insecure_channel", return_value=self.channel_mock):
            self.assertEqual(client.call("make", Empty()), "Espresso!")

        calls = [
            call(self.channel_mock),
            call().make(Empty(), timeout=None),
            call().make(Empty(), timeout=None),
            call().make(Empty(), timeout=None),
            call().make(Empty(), timeout=None),
        ]
        self.assertEqual(self.stub_mock.mock_calls, calls)

    def test_call_grpc_unavailable_too_many_times(self):
        client = GrpcClient(sentinel.netloc, stub_cls=self.stub_mock)
        state = grpc._channel._RPCState((), "", "", grpc.StatusCode.UNAVAILABLE, "Connection reset by peer")
        error = _Rendezvous(state, None, None, None)
        reply = Reply()
        reply.data.result = "Espresso!"
        self.stub_mock.return_value.make.side_effect = error

        with patch("frgal.client.grpc.insecure_channel", return_value=self.channel_mock):
            with self.assertRaises(grpc.RpcError):
                client.call("make", Empty(), max_retries=3)

        calls = [
            call(self.channel_mock),
            call().make(Empty(), timeout=None),
            call().make(Empty(), timeout=None),
            call().make(Empty(), timeout=None),
        ]
        self.assertEqual(self.stub_mock.mock_calls, calls)

    def test_call_grpc_other_error(self):
        client = GrpcClient(sentinel.netloc, stub_cls=self.stub_mock)
        state = grpc._channel._RPCState((), "", "", grpc.StatusCode.UNKNOWN, "Unknown error")
        error = _Rendezvous(state, None, None, None)
        self.stub_mock.return_value.make.side_effect = error

        with patch("frgal.client.grpc.insecure_channel", return_value=self.channel_mock):
            with self.assertRaisesRegex(_Rendezvous, "status = StatusCode.UNKNOWN"):
                client.call("make", Empty())

    def test_call_modify_result(self):
        client = GrpcClient(sentinel.netloc, stub_cls=self.stub_mock)
        reply = Reply()
        reply.data.result = "Espresso!"
        self.stub_mock.return_value.make.return_value = reply

        def modify_result(result):
            return "{} Shaken, not stirred.".format(result)

        with patch("frgal.client.grpc.insecure_channel", return_value=self.channel_mock):
            self.assertEqual(
                client.call("make", Empty(), modify_result=modify_result), "Espresso! Shaken, not stirred."
            )

    def _test_call_future(self, *args: Any, result_value: Any = "Espresso!", **kwargs: Any) -> FutureCallWrapper:
        client = GrpcClient(sentinel.netloc, stub_cls=self.stub_mock)
        reply = Reply()
        reply.data.result = "Espresso!"
        self.stub_mock.return_value.make.future.return_value.result.return_value = reply
        self.stub_mock.return_value.make.future.return_value.code.return_value = sentinel.code

        with self.assertWarnsRegex(
            DeprecationWarning, "Method call_future is deprecated and will be removed. Use async instead."
        ):
            with patch("frgal.client.grpc.insecure_channel", return_value=self.channel_mock):
                result = client.call_future(*args, **kwargs)

        self.assertEqual(result.call, self.stub_mock.return_value.make)
        self.assertEqual(result.request, sentinel.request)
        self.assertEqual(result.result(), result_value)
        self.assertEqual(result.code(), sentinel.code)

        return result

    def test_call_future(self):
        data: Iterable[tuple[tuple, dict]] = (
            # args, kwargs
            (("make", sentinel.request), {}),
            (("make",), {"request": sentinel.request}),
            ((), {"method": "make", "request": sentinel.request}),
        )
        for args, kwargs in data:
            with self.subTest(args=args, kwargs=kwargs):
                self._test_call_future(*args, **kwargs)

    def test_call_future_max_retries(self):
        result = self._test_call_future("make", sentinel.request, max_retries=sentinel.retries)
        self.assertEqual(result.max_retries, sentinel.retries)

    def test_call_future_modify_result(self):
        def modify_result(*args, **kwargs):
            return "Transmogrified!"

        result = self._test_call_future(
            "make", sentinel.request, modify_result=modify_result, result_value="Transmogrified!"
        )
        self.assertEqual(result.modify_result, modify_result)

    def test_call_future_timeout(self):
        data: Iterable[tuple[dict, dict, Any]] = (
            # client_kwargs, call_kwargs, timeout
            ({}, {}, None),
            ({"timeout": sentinel.timeout}, {}, sentinel.timeout),
            ({}, {"timeout": sentinel.timeout}, sentinel.timeout),
            ({"timeout": sentinel.default}, {"timeout": sentinel.timeout}, sentinel.timeout),
        )
        for client_kwargs, call_kwargs, timeout in data:
            with self.subTest(client_kwargs=client_kwargs, call_kwargs=call_kwargs):
                stub_mock = Mock()
                client = GrpcClient(sentinel.netloc, stub_cls=stub_mock, **client_kwargs)
                reply = Reply()
                reply.data.result = "Espresso!"
                future_mock = Mock(autospec=grpc.Future)
                future_mock.result.return_value = reply
                future_mock.code.return_value = "OK"
                stub_mock.return_value.make.future.return_value = future_mock
                stub_mock.return_value.make._method = b"make"

                with self.assertWarnsRegex(
                    DeprecationWarning, "Method call_future is deprecated and will be removed. Use async instead."
                ):
                    with patch("frgal.client.grpc.insecure_channel", return_value=self.channel_mock):
                        self.assertEqual(client.call_future("make", Empty(), **call_kwargs).result(), "Espresso!")

                self.assertEqual(
                    stub_mock.return_value.mock_calls,
                    [call.make.future(Empty(), timeout=timeout), call.make.future().result()],
                )

    def test_call_stream(self):
        data: Iterable[tuple[tuple, dict]] = (
            # args, kwargs
            (("make", sentinel.request), {}),
            (("make",), {"request": sentinel.request}),
            ((), {"method": "make", "request": sentinel.request}),
        )
        for args, kwargs in data:
            with self.subTest(args=args, kwargs=kwargs):
                stub_mock = Mock()
                client = GrpcClient(sentinel.netloc, stub_cls=stub_mock)
                reply = Reply()
                reply.data.result = "Espresso!"
                stub_mock.return_value.make.return_value = [reply]
                stub_mock.return_value.make._method = b"make"

                with patch("frgal.client.grpc.insecure_channel", return_value=self.channel_mock):
                    result = client.call_stream(*args, **kwargs)

                self.assertIsInstance(result, types.GeneratorType)
                self.assertEqual(list(result), ["Espresso!"])
                self.assertEqual(stub_mock.return_value.mock_calls, [call.make(sentinel.request, timeout=None)])

    def test_call_stream_timeout(self):
        data: Iterable[tuple[dict, dict, Any]] = (
            # client_kwargs, call_kwargs, timeout
            ({}, {}, None),
            ({"timeout": sentinel.timeout}, {}, sentinel.timeout),
            ({}, {"timeout": sentinel.timeout}, sentinel.timeout),
            ({"timeout": sentinel.default}, {"timeout": sentinel.timeout}, sentinel.timeout),
        )
        for client_kwargs, call_kwargs, timeout in data:
            with self.subTest(client_kwargs=client_kwargs, call_kwargs=call_kwargs):
                stub_mock = Mock()
                client = GrpcClient(sentinel.netloc, stub_cls=stub_mock, **client_kwargs)
                reply = Reply()
                reply.data.result = "Espresso!"
                stub_mock.return_value.make.return_value = [reply]
                stub_mock.return_value.make._method = b"make"

                with patch("frgal.client.grpc.insecure_channel", return_value=self.channel_mock):
                    self.assertEqual(list(client.call_stream("make", Empty(), **call_kwargs)), ["Espresso!"])

                self.assertEqual(stub_mock.return_value.mock_calls, [call.make(Empty(), timeout=timeout)])

    def test_call_stream__empty_empty(self):
        self.stub_mock.return_value.make.return_value = [Empty(), Empty(), Empty()]

        with patch("frgal.client.get_call_id", return_value="XXXX"):
            self.assertEqual(list(self.client.call_stream("make", Empty())), [None, None, None])

        self.log_handler.check(
            ("frgal.client", "DEBUG", "[XXXX] make started"),
            ("frgal.client", "DEBUG", "[XXXX] make()"),
            ("frgal.client", "DEBUG", "[XXXX] make returned "),
            ("frgal.client", "DEBUG", "[XXXX] make returned "),
            ("frgal.client", "DEBUG", "[XXXX] make returned "),
            ("frgal.client", "DEBUG", "[XXXX] make returned 3 chunks"),
        )

    def test_call_stream__unary(self):
        request = SimpleMessage(value="Answers!")
        self.stub_mock.return_value.make.return_value = [make_reply("42"), make_reply("43"), make_reply("44")]

        with patch("frgal.client.get_call_id", return_value="XXXX"):
            self.assertEqual(list(self.client.call_stream("make", request)), ["42", "43", "44"])

        self.log_handler.check(
            ("frgal.client", "DEBUG", "[XXXX] make started"),
            ("frgal.client", "DEBUG", '[XXXX] make(value: "Answers!")'),
            ("frgal.client", "DEBUG", '[XXXX] make returned data { result: "42" }'),
            ("frgal.client", "DEBUG", '[XXXX] make returned data { result: "43" }'),
            ("frgal.client", "DEBUG", '[XXXX] make returned data { result: "44" }'),
            ("frgal.client", "DEBUG", "[XXXX] make returned 3 chunks"),
        )

    def test_call_stream__stream(self):
        requests = iter((SimpleMessage(value="Lister"), SimpleMessage(value="Rimmer"), SimpleMessage(value="Kryten")))

        # Mock method to consumer the requests
        def _make(requests, *args, **kwargs):
            tuple(requests)
            yield make_reply("42")
            yield make_reply("43")
            yield make_reply("44")

        self.stub_mock.return_value.make.side_effect = _make

        with patch("frgal.client.get_call_id", return_value="XXXX"):
            self.assertEqual(list(self.client.call_stream("make", requests)), ["42", "43", "44"])

        self.log_handler.check(
            ("frgal.client", "DEBUG", "[XXXX] make started"),
            ("frgal.client", "DEBUG", '[XXXX] make(value: "Lister")'),
            ("frgal.client", "DEBUG", '[XXXX] make(value: "Rimmer")'),
            ("frgal.client", "DEBUG", '[XXXX] make(value: "Kryten")'),
            ("frgal.client", "DEBUG", '[XXXX] make returned data { result: "42" }'),
            ("frgal.client", "DEBUG", '[XXXX] make returned data { result: "43" }'),
            ("frgal.client", "DEBUG", '[XXXX] make returned data { result: "44" }'),
            ("frgal.client", "DEBUG", "[XXXX] make returned 3 chunks"),
        )

    def test_call_stream__max_count(self):
        request = SimpleMessage(value="Answers!")
        self.stub_mock.return_value.make.return_value = [make_reply("42"), make_reply("43"), make_reply("44")]

        with patch("frgal.client.MESSAGE_MAX_COUNT", 2):
            with patch("frgal.client.get_call_id", return_value="XXXX"):
                self.assertEqual(list(self.client.call_stream("make", request)), ["42", "43", "44"])

        self.log_handler.check(
            ("frgal.client", "DEBUG", "[XXXX] make started"),
            ("frgal.client", "DEBUG", '[XXXX] make(value: "Answers!")'),
            ("frgal.client", "DEBUG", '[XXXX] make returned data { result: "42" }'),
            ("frgal.client", "DEBUG", '[XXXX] make returned data { result: "43" }'),
            ("frgal.client", "DEBUG", "[XXXX] make returned 3 chunks"),
        )

    def test_decoder_default(self):
        client = GrpcClient(sentinel.netloc, stub_cls=sentinel.stub)

        self.assertEqual(type(client.decoder), GrpcDecoder)

    def test_decoder_custom(self):
        client = GrpcClient(sentinel.netloc, stub_cls=sentinel.stub, decoder=sentinel.decoder)

        self.assertEqual(client.decoder, sentinel.decoder)

    def test_decoder_class(self):
        class TestDecoder(GrpcDecoder):
            pass

        class TestClient(GrpcClient):
            decoder_cls = TestDecoder

        client = TestClient(sentinel.netloc, stub_cls=sentinel.stub)

        self.assertIsInstance(client.decoder, TestDecoder)

    def test_options_default(self):
        client = GrpcClient(sentinel.netloc, stub_cls=sentinel.stub)

        self.assertEqual(client.options, dict(self.default_options))

    def test_options_custom(self):
        client = GrpcClient(sentinel.netloc, stub_cls=sentinel.stub, options={"key": sentinel.value})

        options = {
            "grpc.min_reconnect_backoff_ms": GRPC_MIN_RECONNECT_BACKOFF_MS,
            "grpc.max_reconnect_backoff_ms": GRPC_MAX_RECONNECT_BACKOFF_MS,
            "key": sentinel.value,
        }
        self.assertEqual(client.options, options)

    def test_options_class(self):
        class TestClient(GrpcClient):
            options = {"key": sentinel.value}

        client = TestClient(sentinel.netloc, stub_cls=sentinel.stub)

        self.assertEqual(client.options, {"key": sentinel.value})

    def test_options_update(self):
        class TestClient(GrpcClient):
            options = {"key": sentinel.value}

        client = TestClient(sentinel.netloc, stub_cls=sentinel.stub, options={"key2": sentinel.value2})

        self.assertEqual(client.options, {"key": sentinel.value, "key2": sentinel.value2})

    def test_options_override(self):
        class TestClient(GrpcClient):
            options = {"key": sentinel.value}

        client = TestClient(sentinel.netloc, stub_cls=sentinel.stub, options={"key": sentinel.value2})

        self.assertEqual(client.options, {"key": sentinel.value2})

    def test_stub_cls(self):
        class TestClient(GrpcClient):
            stub_cls = sentinel.cls_stub

        data: tuple[tuple[Callable, Any], ...] = (
            # client_cls, stub_cls
            (partial(GrpcClient, stub_cls=sentinel.stub_cls), sentinel.stub_cls),
            # Client with class default
            (TestClient, sentinel.cls_stub),
            # Override class defaults by instance defaults.
            (partial(TestClient, stub_cls=sentinel.stub_cls), sentinel.stub_cls),
            (partial(TestClient, stub_cls=None), sentinel.cls_stub),
        )
        for client_cls, stub_cls in data:
            with self.subTest(client_cls=client_cls):
                client = client_cls(sentinel.netloc)

                self.assertEqual(client.stub_cls, stub_cls)

    def test_timeout(self):
        class TestClient(GrpcClient):
            timeout = sentinel.cls_timeout

        data: tuple[tuple[Callable, Any], ...] = (
            # client_cls, timeout
            (GrpcClient, None),
            (partial(GrpcClient, timeout=sentinel.timeout), sentinel.timeout),
            # Client with class default
            (TestClient, sentinel.cls_timeout),
            # Override class defaults by instance defaults.
            (partial(TestClient, timeout=sentinel.timeout), sentinel.timeout),
            (partial(TestClient, timeout=None), None),
        )
        for client_cls, timeout in data:
            with self.subTest(client_cls=client_cls):
                with patch("frgal.client.grpc.insecure_channel"):
                    client = client_cls(sentinel.netloc, stub_cls=sentinel.stub)

                self.assertEqual(client.timeout, timeout)


class MakeCredentialsTest(TestCase):
    def setUp(self):
        self.temp_dir = TempDirectory()
        self.temp_dir.write("ssl_cert.pem", b"SSL CERT CONTENT")

    def tearDown(self):
        self.temp_dir.cleanup()

    def test_file(self):
        with patch("frgal.client.ssl_channel_credentials") as cred_mock:
            cred_mock.return_value = sentinel.credentials

            result = make_credentials(self.temp_dir.getpath("ssl_cert.pem"))

        self.assertEqual(result, sentinel.credentials)
        self.assertEqual(cred_mock.mock_calls, [call("SSL CERT CONTENT")])

    def test_none(self):
        self.assertIsNone(make_credentials(None))
