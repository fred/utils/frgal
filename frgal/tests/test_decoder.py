"""Tests for decoder module."""

from datetime import datetime, timezone
from enum import Enum, IntEnum
from typing import Any, cast
from unittest import TestCase

from google.protobuf.empty_pb2 import Empty
from google.protobuf.struct_pb2 import Struct
from google.protobuf.timestamp_pb2 import Timestamp
from testfixtures import ShouldWarn

from frgal.decoder import GrpcDecoder
from frgal.exceptions import DecoderError, UnknownError

from .proto.example_pb2 import (  # type: ignore[attr-defined]
    ComplexMessage,
    ComplexNestedMessage,
    ComplexOneofMessage,
    ComplexOptionalMessage,
    CompositeDictMessage,
    CompositeListMessage,
    DictMessage,
    Direction,
    EnumMessage,
    ListMessage,
    NestedMessage,
    OptionalEnumMessage,
    Reply,
    SimpleMessage,
)


class GrpcDecoderDecodeNotSetTest(TestCase):
    """Test GrpcDecoder."""

    def setUp(self):
        self.decoder = GrpcDecoder()

    # Test various message structures
    def test_decode_empty(self):
        self.assertIsNone(self.decoder.decode(Empty()))

    def test_decode_empty_message(self):
        self.assertEqual(self.decoder.decode(SimpleMessage()), "")

    def test_decode_onefield_message(self):
        msg = SimpleMessage()
        msg.value = "Arnold Rimmer"

        self.assertEqual(self.decoder.decode(msg), "Arnold Rimmer")

    def test_decode_complex_message(self):
        msg = ComplexMessage()
        msg.value = "Arnold Rimmer"
        msg.count = 42

        self.assertEqual(self.decoder.decode(msg), {"value": "Arnold Rimmer", "count": 42})

    def test_decode_str_empty(self):
        data = (
            (SimpleMessage(value=""), ""),
            (ComplexMessage(), {"value": "", "count": 0}),
        )
        for message, output in data:
            with self.subTest(message=message, output=output):
                self.assertEqual(self.decoder.decode(message), output)

    def test_decode_nested_empty(self):
        msg = NestedMessage()

        self.assertEqual(self.decoder.decode(msg), None)

    def test_decode_nested(self):
        msg = NestedMessage()
        msg.value.value = "Arnold"

        self.assertEqual(self.decoder.decode(msg), "Arnold")

    def test_decode_complex_nested_empty(self):
        msg = ComplexNestedMessage()

        self.assertEqual(self.decoder.decode(msg), {"value1": None, "value2": None})

    def test_decode_with_primitive_type_default_values(self):
        self.assertEqual(self.decoder.decode(ComplexMessage()), {"value": "", "count": 0})

    # Test various value types
    def test_decode_list_empty(self):
        msg = ListMessage()
        self.assertEqual(self.decoder.decode(msg), [])

    def test_decode_list(self):
        msg = ListMessage()
        msg.values.append("Arnold")
        msg.values.append("Lister")

        self.assertEqual(self.decoder.decode(msg), ["Arnold", "Lister"])

    def test_decode_composite_list_empty(self):
        msg = CompositeListMessage()
        self.assertEqual(self.decoder.decode(msg), [])

    def test_decode_composite_list(self):
        msg = CompositeListMessage()
        arnold = SimpleMessage()
        arnold.value = "Arnold"
        lister = SimpleMessage()
        lister.value = "Lister"
        msg.values.MergeFrom([arnold, lister])

        self.assertEqual(self.decoder.decode(msg), ["Arnold", "Lister"])

    def test_decode_dict_empty(self):
        msg = DictMessage()
        self.assertEqual(self.decoder.decode(msg), {})

    def test_decode_dict(self):
        msg = DictMessage()
        msg.values["arnold"] = "Arnold"
        msg.values["lister"] = "Lister"

        self.assertEqual(self.decoder.decode(msg), {"arnold": "Arnold", "lister": "Lister"})

    def test_decode_composite_dict_empty(self):
        msg = CompositeDictMessage()
        self.assertEqual(self.decoder.decode(msg), {})

    def test_decode_composite_dict(self):
        msg = CompositeDictMessage()
        msg.values["arnold"].value = "Arnold"
        msg.values["lister"].value = "Lister"

        self.assertEqual(self.decoder.decode(msg), {"arnold": "Arnold", "lister": "Lister"})

    def test_decode_unknown_type(self):
        with self.assertRaises(DecoderError):
            (self.decoder.decode(object()),)

    def test_decode_timestamp(self):
        self.assertIsNone(self.decoder.decode(Timestamp()))

        self.assertEqual(self.decoder.decode(Timestamp(nanos=1)), datetime(1970, 1, 1, 0, 0, 0, 0, tzinfo=timezone.utc))

        self.assertEqual(
            self.decoder.decode(Timestamp(seconds=1)), datetime(1970, 1, 1, 0, 0, 1, 0, tzinfo=timezone.utc)
        )

        self.assertEqual(
            self.decoder.decode(Timestamp(seconds=123456789, nanos=12345678)),
            datetime(1973, 11, 29, 21, 33, 9, 12345, tzinfo=timezone.utc),
        )

    def test_decode_oneof_empty(self):
        message = ComplexOneofMessage()
        self.assertEqual(self.decoder.decode(message), {})

    def test_decode_oneof_full(self):
        message = ComplexOneofMessage()
        message.value = 0
        message.value2 = 2
        self.assertEqual(self.decoder.decode(message), {"value": 0, "value2": 2})

    def test_decode_oneof(self):
        # Test decoding of oneof fields
        reply = Reply()
        reply.data.result = "Gazpacho!"

        self.assertEqual(self.decoder.decode(reply), {"data": "Gazpacho!"})

    def test_decode_struct(self):
        data: list[dict[str, Any]] = [
            {},
            {"null": None},
            {"true": True},
            {"false": False},
            {"zero": 0},
            {"integer": 42},
            {"empty_string": ""},
            {"string": "string"},
        ]
        data += [{"nested_list": list(content.values())} for content in data]
        data += [{"nested_dict": content} for content in data]
        for content in data:
            with self.subTest(content=content):
                struct = Struct()
                struct.update(content)
                self.assertEqual(self.decoder.decode(struct), content)

    # Test exceptions
    def test_decode_exception(self):
        # Add exception decoder
        class TeaPotError(Exception):
            def __init__(self, temperature, plugged_in):
                self.temperature = temperature
                self.plugged_in = plugged_in

        self.decoder.set_exception_decoder(Reply.Exception.TeaPotError, TeaPotError)

        reply = Reply()
        reply.exception.tea_pot_error.CopyFrom(Reply.Exception.TeaPotError(temperature=42, plugged_in=True))
        decoded = cast(TeaPotError, self.decoder.decode_exception(reply.exception))
        self.assertIsInstance(decoded, TeaPotError)
        self.assertEqual(decoded.temperature, 42)
        self.assertTrue(decoded.plugged_in)

    def test_decode_exception_one_field(self):
        # Add exception decoder
        class MagicError(Exception):
            def __init__(self, thaum):
                self.thaum = thaum

        self.decoder.set_exception_decoder(Reply.Exception.MagicError, MagicError)

        error = Reply.Exception()
        error.magic_error.CopyFrom(Reply.Exception.MagicError(thaum=42))
        decoded = cast(MagicError, self.decoder.decode_exception(error))
        self.assertIsInstance(decoded, MagicError)
        self.assertEqual(decoded.thaum, 42)

    def test_decode_exception_empty(self):
        error = Reply.Exception()
        self.assertIsInstance(self.decoder.decode_exception(error), UnknownError)

    def test_decode_exception_unknown(self):
        error = Reply.Exception()
        error.something_went_wrong.CopyFrom(Reply.Exception.SomethingWentWrong())

        exception = self.decoder.decode_exception(error)
        self.assertIsInstance(exception, UnknownError)
        self.assertEqual(exception.args, ("something_went_wrong",))

    def test_decode_exception_no_reason(self):
        error = Reply.Exception()

        exception = self.decoder.decode_exception(error)
        self.assertIsInstance(exception, UnknownError)
        self.assertEqual(exception.args, ("Exception was returned with no reason defined.",))

    def test_optional_fields_full(self):
        message = ComplexOptionalMessage()
        message.value = "kryten"
        message.number = 42
        self.assertEqual(
            self.decoder.decode(message),
            {
                "value": "kryten",
                "number": 42,
            },
        )

    def test_optional_fields_empty(self):
        message = ComplexOptionalMessage()
        self.assertEqual(
            self.decoder.decode(message),
            {
                "value": None,
                "number": None,
            },
        )

    def test_decode_int_enum(self):
        class DirectionEnum(IntEnum):
            UP = 1
            RIGHT = 2
            DOWN = 3
            LEFT = 4

        self.decoder.set_enum_decoder(Direction, DirectionEnum)

        with self.assertRaises(DecoderError):
            self.decoder.decode(EnumMessage())

        self.assertEqual(self.decoder.decode(EnumMessage(direction=1)), DirectionEnum.UP)
        self.assertEqual(self.decoder.decode(EnumMessage(direction=4)), DirectionEnum.LEFT)

        with self.assertRaises(DecoderError):
            self.decoder.decode(EnumMessage(direction=5))

    def test_decode_str_enum(self):
        class DirectionEnum(str, Enum):
            UP = "up"
            RIGHT = "right"
            DOWN = "down"
            LEFT = "left"

        self.decoder.set_enum_decoder(Direction, DirectionEnum)

        with self.assertRaises(DecoderError):
            self.decoder.decode(EnumMessage())

        self.assertEqual(self.decoder.decode(EnumMessage(direction=1)), DirectionEnum.UP)
        self.assertEqual(self.decoder.decode(EnumMessage(direction=4)), DirectionEnum.LEFT)

        with self.assertRaises(DecoderError):
            self.decoder.decode(EnumMessage(direction=5))

    def test_optional_enum(self):
        class DirectionEnum(IntEnum):
            UP = 1
            RIGHT = 2
            DOWN = 3
            LEFT = 4

        self.decoder.set_enum_decoder(Direction, DirectionEnum)

        self.assertIsNone(self.decoder.decode(OptionalEnumMessage()))
        self.assertEqual(self.decoder.decode(OptionalEnumMessage(direction=1)), DirectionEnum.UP)
        self.assertEqual(self.decoder.decode(OptionalEnumMessage(direction=4)), DirectionEnum.LEFT)

    def test_decode_enum_as_int(self):
        warning = DeprecationWarning("Decoding Enum as int is deprecated and will be removed in the future.")
        data = (
            (EnumMessage(), 0),
            (EnumMessage(direction=1), 1),
            (EnumMessage(direction=4), 4),
        )
        for message, value in data:
            with self.subTest(message=message, value=value):
                with ShouldWarn(warning):
                    self.assertEqual(self.decoder.decode(message), value)

    def test_set_enum_decoder_unknown_type(self):
        class MyEnum(Enum):
            A = 1

        with self.assertRaises(ValueError):
            self.decoder.set_enum_decoder(Direction, MyEnum)
