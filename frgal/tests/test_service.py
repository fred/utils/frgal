from collections.abc import AsyncIterator, Iterator
from typing import NoReturn
from unittest import IsolatedAsyncioTestCase, TestCase
from unittest.mock import AsyncMock, Mock, call, sentinel

from asyncstdlib import list as alist
from google.protobuf.message import Message
from grpc import ServicerContext
from grpc.aio import ServicerContext as AsyncServicerContext
from testfixtures import LogCapture

from frgal.exceptions import ServiceError
from frgal.service import awrap_stream, awrap_unary, wrap_stream, wrap_unary


class WrapUnaryTest(TestCase):
    def setUp(self):
        self.log_handler = LogCapture()

    def tearDown(self):
        self.log_handler.uninstall()

    def test_call_unary_unary(self):
        class TestServicer:
            @wrap_unary
            def serve(self, request: Message, context: ServicerContext) -> Message:
                return sentinel.response  # type: ignore[no-any-return]

        servicer = TestServicer()

        response = servicer.serve(sentinel.request, sentinel.context)

        self.assertEqual(response, sentinel.response)
        # Check logs
        self.assertEqual(len(self.log_handler.records), 2)
        self.assertRegex(self.log_handler.records[0].getMessage(), r"\[\w{4}\] .*TestServicer.serve(.*)")
        self.assertRegex(self.log_handler.records[1].getMessage(), r"\[\w{4}\] .*TestServicer.serve returned .*")

    def test_call_stream_unary(self):
        class TestServicer:
            @wrap_unary
            def serve(self, request: Iterator[Message], context: ServicerContext) -> Message:
                return sentinel.response  # type: ignore[no-any-return]

        servicer = TestServicer()

        response = servicer.serve(iter([sentinel.request] * 5), sentinel.context)

        self.assertEqual(response, sentinel.response)
        # Check logs
        self.assertEqual(len(self.log_handler.records), 2)
        self.assertRegex(self.log_handler.records[0].getMessage(), r"\[\w{4}\] .*TestServicer.serve(.*)")
        self.assertRegex(self.log_handler.records[1].getMessage(), r"\[\w{4}\] .*TestServicer.serve returned .*")

    def test_service_error(self):
        class TestServicer:
            @wrap_unary
            def serve(self, request: Message, context: ServicerContext) -> NoReturn:
                raise ServiceError(sentinel.status, message=sentinel.message)

        servicer = TestServicer()
        context = Mock(ServicerContext)
        context.abort.side_effect = Exception("Abort!")

        with self.assertRaisesRegex(Exception, "Abort!"):
            servicer.serve(sentinel.request, context)

        self.assertEqual(context.mock_calls, [call.abort(sentinel.status, sentinel.message)])
        # Check logs
        self.assertEqual(len(self.log_handler.records), 2)
        self.assertRegex(self.log_handler.records[0].getMessage(), r"\[\w{4}\] .*TestServicer.serve(.*)")
        self.assertRegex(self.log_handler.records[1].getMessage(), r"\[\w{4}\] .*TestServicer.serve failed with .*")

    def test_error(self):
        class TestServicer:
            @wrap_unary
            def serve(self, request: Message, context: ServicerContext) -> NoReturn:
                raise Exception("Smeghead!")

        servicer = TestServicer()

        with self.assertRaisesRegex(Exception, "Smeghead!"):
            servicer.serve(sentinel.request, sentinel.context)

        # Check logs
        self.assertEqual(len(self.log_handler.records), 2)
        self.assertRegex(self.log_handler.records[0].getMessage(), r"\[\w{4}\] .*TestServicer.serve(.*)")
        self.assertRegex(self.log_handler.records[1].getMessage(), r"\[\w{4}\] .*TestServicer.serve raised error .*")


class AwrapUnaryTest(IsolatedAsyncioTestCase):
    def setUp(self):
        self.log_handler = LogCapture()

    def tearDown(self):
        self.log_handler.uninstall()

    async def test_call_unary_unary(self):
        class TestServicer:
            @awrap_unary
            async def serve(self, request: Message, context: AsyncServicerContext) -> Message:
                return sentinel.response  # type: ignore[no-any-return]

        servicer = TestServicer()

        response = await servicer.serve(sentinel.request, sentinel.context)

        self.assertEqual(response, sentinel.response)
        # Check logs
        self.assertEqual(len(self.log_handler.records), 2)
        self.assertRegex(self.log_handler.records[0].getMessage(), r"\[\w{4}\] .*TestServicer.serve(.*)")
        self.assertRegex(self.log_handler.records[1].getMessage(), r"\[\w{4}\] .*TestServicer.serve returned .*")

    async def test_call_stream_unary(self):
        class TestServicer:
            @awrap_unary
            async def serve(self, request: Iterator[Message], context: AsyncServicerContext) -> Message:
                return sentinel.response  # type: ignore[no-any-return]

        servicer = TestServicer()

        response = await servicer.serve(iter([sentinel.request] * 5), sentinel.context)

        self.assertEqual(response, sentinel.response)
        # Check logs
        self.assertEqual(len(self.log_handler.records), 2)
        self.assertRegex(self.log_handler.records[0].getMessage(), r"\[\w{4}\] .*TestServicer.serve(.*)")
        self.assertRegex(self.log_handler.records[1].getMessage(), r"\[\w{4}\] .*TestServicer.serve returned .*")

    async def test_service_error(self):
        class TestServicer:
            @awrap_unary
            async def serve(self, request: Message, context: AsyncServicerContext) -> NoReturn:
                raise ServiceError(sentinel.status, message=sentinel.message)

        servicer = TestServicer()
        context = AsyncMock(AsyncServicerContext)
        context.abort.side_effect = Exception("Abort!")

        with self.assertRaisesRegex(Exception, "Abort!"):
            await servicer.serve(sentinel.request, context)

        self.assertEqual(context.mock_calls, [call.abort(sentinel.status, sentinel.message)])
        self.assertEqual(context.abort.await_args_list, [call(sentinel.status, sentinel.message)])

        # Check logs
        self.assertEqual(len(self.log_handler.records), 2)
        self.assertRegex(self.log_handler.records[0].getMessage(), r"\[\w{4}\] .*TestServicer.serve(.*)")
        self.assertRegex(self.log_handler.records[1].getMessage(), r"\[\w{4}\] .*TestServicer.serve failed with .*")

    async def test_error(self):
        class TestServicer:
            @awrap_unary
            async def serve(self, request: Message, context: AsyncServicerContext) -> NoReturn:
                raise Exception("Smeghead!")

        servicer = TestServicer()

        with self.assertRaisesRegex(Exception, "Smeghead!"):
            await servicer.serve(sentinel.request, sentinel.context)

        # Check logs
        self.assertEqual(len(self.log_handler.records), 2)
        self.assertRegex(self.log_handler.records[0].getMessage(), r"\[\w{4}\] .*TestServicer.serve(.*)")
        self.assertRegex(self.log_handler.records[1].getMessage(), r"\[\w{4}\] .*TestServicer.serve raised error .*")


class WrapStreamTest(TestCase):
    def setUp(self):
        self.log_handler = LogCapture()

    def tearDown(self):
        self.log_handler.uninstall()

    def test_call_unary_stream(self):
        class TestServicer:
            @wrap_stream
            def serve(self, request: Message, context: ServicerContext) -> Iterator[Message]:
                for _ in range(5):
                    yield sentinel.response

        servicer = TestServicer()

        response = servicer.serve(sentinel.request, sentinel.context)

        self.assertEqual(list(response), [sentinel.response] * 5)
        # Check logs
        self.assertEqual(len(self.log_handler.records), 6)
        self.assertRegex(self.log_handler.records[0].getMessage(), r"\[\w{4}\] .*TestServicer.serve(.*)")
        for i in range(5):
            self.assertRegex(
                self.log_handler.records[i + 1].getMessage(), r"\[\w{4}\] .*TestServicer.serve returned .*"
            )

    def test_call_stream_stream(self):
        class TestServicer:
            @wrap_stream
            def serve(self, request: Iterator[Message], context: ServicerContext) -> Iterator[Message]:
                for _ in range(5):
                    yield sentinel.response

        servicer = TestServicer()

        response = servicer.serve(iter([sentinel.request] * 5), sentinel.context)

        self.assertEqual(list(response), [sentinel.response] * 5)
        # Check logs
        self.assertEqual(len(self.log_handler.records), 6)
        self.assertRegex(self.log_handler.records[0].getMessage(), r"\[\w{4}\] .*TestServicer.serve(.*)")
        for i in range(5):
            self.assertRegex(
                self.log_handler.records[i + 1].getMessage(), r"\[\w{4}\] .*TestServicer.serve returned .*"
            )

    def test_service_error(self):
        class TestServicer:
            @wrap_stream
            def serve(self, request: Message, context: ServicerContext) -> Iterator[Message]:
                raise ServiceError(sentinel.status, message=sentinel.message)

        servicer = TestServicer()
        context = Mock(ServicerContext)
        context.abort.side_effect = Exception("Abort!")

        response = servicer.serve(sentinel.request, context)
        with self.assertRaisesRegex(Exception, "Abort!"):
            list(response)

        self.assertEqual(context.mock_calls, [call.abort(sentinel.status, sentinel.message)])
        # Check logs
        self.assertEqual(len(self.log_handler.records), 2)
        self.assertRegex(self.log_handler.records[0].getMessage(), r"\[\w{4}\] .*TestServicer.serve(.*)")
        self.assertRegex(self.log_handler.records[1].getMessage(), r"\[\w{4}\] .*TestServicer.serve failed with .*")

    def test_error(self):
        class TestServicer:
            @wrap_stream
            def serve(self, request: Message, context: ServicerContext) -> Iterator[Message]:
                raise Exception("Smeghead!")

        servicer = TestServicer()

        response = servicer.serve(sentinel.request, sentinel.context)
        with self.assertRaisesRegex(Exception, "Smeghead!"):
            list(response)

        # Check logs
        self.assertEqual(len(self.log_handler.records), 2)
        self.assertRegex(self.log_handler.records[0].getMessage(), r"\[\w{4}\] .*TestServicer.serve(.*)")
        self.assertRegex(self.log_handler.records[1].getMessage(), r"\[\w{4}\] .*TestServicer.serve raised error .*")


class AwrapStreamTest(IsolatedAsyncioTestCase):
    def setUp(self):
        self.log_handler = LogCapture()

    def tearDown(self):
        self.log_handler.uninstall()

    async def test_call_unary_stream(self):
        class TestServicer:
            @awrap_stream
            async def serve(self, request: Message, context: AsyncServicerContext) -> AsyncIterator[Message]:
                for _ in range(5):
                    yield sentinel.response

        servicer = TestServicer()

        response = servicer.serve(sentinel.request, sentinel.context)

        self.assertEqual(await alist(response), [sentinel.response] * 5)
        # Check logs
        self.assertEqual(len(self.log_handler.records), 6)
        self.assertRegex(self.log_handler.records[0].getMessage(), r"\[\w{4}\] .*TestServicer.serve(.*)")
        for i in range(5):
            self.assertRegex(
                self.log_handler.records[i + 1].getMessage(), r"\[\w{4}\] .*TestServicer.serve returned .*"
            )

    async def test_call_stream_stream(self):
        class TestServicer:
            @awrap_stream
            async def serve(self, request: Iterator[Message], context: AsyncServicerContext) -> AsyncIterator[Message]:
                for _ in range(5):
                    yield sentinel.response

        servicer = TestServicer()

        response = servicer.serve(iter([sentinel.request] * 5), sentinel.context)

        self.assertEqual(await alist(response), [sentinel.response] * 5)
        # Check logs
        self.assertEqual(len(self.log_handler.records), 6)
        self.assertRegex(self.log_handler.records[0].getMessage(), r"\[\w{4}\] .*TestServicer.serve(.*)")
        for i in range(5):
            self.assertRegex(
                self.log_handler.records[i + 1].getMessage(), r"\[\w{4}\] .*TestServicer.serve returned .*"
            )

    async def test_service_error(self):
        class TestServicer:
            @awrap_stream
            async def serve(self, request: Message, context: AsyncServicerContext) -> AsyncIterator[Message]:
                raise ServiceError(sentinel.status, message=sentinel.message)
                # make async generator
                yield sentinel.response  # type: ignore[unreachable] # pragma: no cover

        servicer = TestServicer()
        context = AsyncMock(AsyncServicerContext)
        context.abort.side_effect = Exception("Abort!")

        response = servicer.serve(sentinel.request, context)
        with self.assertRaisesRegex(Exception, "Abort!"):
            await alist(response)

        self.assertEqual(context.mock_calls, [call.abort(sentinel.status, sentinel.message)])
        self.assertEqual(context.abort.await_args_list, [call(sentinel.status, sentinel.message)])
        # Check logs
        self.assertEqual(len(self.log_handler.records), 2)
        self.assertRegex(self.log_handler.records[0].getMessage(), r"\[\w{4}\] .*TestServicer.serve(.*)")
        self.assertRegex(self.log_handler.records[1].getMessage(), r"\[\w{4}\] .*TestServicer.serve failed with .*")

    async def test_error(self):
        class TestServicer:
            @awrap_stream
            async def serve(self, request: Message, context: AsyncServicerContext) -> AsyncIterator[Message]:
                raise Exception("Smeghead!")
                # make async generator
                yield sentinel.response  # type: ignore[unreachable] # pragma: no cover

        servicer = TestServicer()

        response = servicer.serve(sentinel.request, sentinel.context)
        with self.assertRaisesRegex(Exception, "Smeghead!"):
            await alist(response)

        # Check logs
        self.assertEqual(len(self.log_handler.records), 2)
        self.assertRegex(self.log_handler.records[0].getMessage(), r"\[\w{4}\] .*TestServicer.serve(.*)")
        self.assertRegex(self.log_handler.records[1].getMessage(), r"\[\w{4}\] .*TestServicer.serve raised error .*")
