import logging
from unittest import IsolatedAsyncioTestCase, TestCase
from unittest.mock import patch

from asyncstdlib import anext, iter as aiter, tuple as atuple
from testfixtures import LogCapture

from frgal._utils import MESSAGE_MAX_LENGTH, LogRequestStreamWrapper, format_message, get_call_id, lazy_format_message

from .proto.example_pb2 import SimpleMessage  # type: ignore[attr-defined]


class GetCallIdTest(TestCase):
    def test_get_call_id(self):
        call_id = get_call_id()

        self.assertIsInstance(call_id, str)
        self.assertEqual(len(call_id), 4)

    def test_random(self):
        self.assertNotEqual(get_call_id(), get_call_id())


class FormatMessageTest(TestCase):
    def test_short_message(self):
        message = SimpleMessage()
        message.value = "I am fish!"
        self.assertEqual(format_message(message), 'value: "I am fish!"')

    def test_long_message(self):
        message = SimpleMessage()
        message.value = "I am fish! " * 400

        out = format_message(message)

        self.assertEqual(len(out), MESSAGE_MAX_LENGTH)
        self.assertRegex(out, r'^value: "I am fish! .*\.\.\.$')

    def test_short_object(self):
        self.assertEqual(format_message(42), "42")

    def test_long_object(self):
        message = ["I am fish!"] * 400
        self.assertRegex(format_message(message), r"^\['I am fish!', .*\.\.\.$")

    def test_generator(self):
        obj = ("I am fish!" for i in range(400))  # pragma: no cover
        self.assertRegex(format_message(obj), r"^<generator object .*>$")


class LazyFormatMessageTest(TestCase):
    def test(self):
        # Test evaluation of the lazy wrapper.
        data = (
            (SimpleMessage(value="I am fish!"), 'value: "I am fish!"'),
            (42, "42"),
        )
        for message, result in data:
            with self.subTest(message=message):
                self.assertEqual(str(lazy_format_message(message)), result)
                self.assertEqual(repr(lazy_format_message(message)), result)
                self.assertEqual("{}".format(lazy_format_message(message)), result)
                self.assertEqual("{}".format(lazy_format_message(message)), result)
                self.assertEqual("{!r}".format(lazy_format_message(message)), result)

    def test_shorten(self):
        # Test message shortening
        data = (
            (SimpleMessage(value="I am fish!" * 400), 21, 'value: "I am fish!...'),
            (["I am fish!"] * 400, 18, "['I am fish!', ..."),
        )
        for message, max_length, result in data:
            with self.subTest(message=message, max_length=max_length):
                with patch("frgal._utils.MESSAGE_MAX_LENGTH", new=max_length):
                    self.assertEqual(str(lazy_format_message(message)), result)
                    self.assertEqual(repr(lazy_format_message(message)), result)
                    self.assertEqual("{}".format(lazy_format_message(message)), result)
                    self.assertEqual("{}".format(lazy_format_message(message)), result)
                    self.assertEqual("{!r}".format(lazy_format_message(message)), result)


class LogRequestStreamWrapperTest(IsolatedAsyncioTestCase):
    logger = logging.getLogger(__name__)
    call_id = "2X4B"
    method = "wash_dishes"

    def setUp(self):
        self.log_handler = LogCapture()
        self.addCleanup(self.log_handler.uninstall)

    def test_iter(self):
        wrapper = LogRequestStreamWrapper(range(5), self.logger, self.call_id, self.method)

        self.assertIs(iter(wrapper), wrapper)

        self.assertEqual(len(self.log_handler.records), 0)

    def test_next(self):
        wrapper = LogRequestStreamWrapper(range(5), self.logger, self.call_id, self.method)

        self.assertEqual(next(wrapper), 0)
        self.assertEqual(next(wrapper), 1)

        self.assertEqual(len(self.log_handler.records), 2)
        self.assertEqual(self.log_handler.records[0].getMessage(), "[2X4B] wash_dishes(0)")
        self.assertEqual(self.log_handler.records[1].getMessage(), "[2X4B] wash_dishes(1)")

    def test_next_stopped(self):
        wrapper = LogRequestStreamWrapper[int]((), self.logger, self.call_id, self.method)
        with self.assertRaises(StopIteration):
            next(wrapper)

        self.assertEqual(len(self.log_handler.records), 0)

    def test_consume(self):
        # A set of test to consumer the iterator.
        data = (
            # input, output
            ((), ()),
            (range(5), tuple(range(5))),
            (tuple(range(5)), tuple(range(5))),
        )
        for input, output in data:
            with self.subTest(input=input):
                wrapper = LogRequestStreamWrapper(input, self.logger, self.call_id, self.method)
                self.assertEqual(tuple(wrapper), output)

    def test_log_max_count(self):
        with patch("frgal._utils.MESSAGE_MAX_COUNT", 2):
            wrapper = LogRequestStreamWrapper(range(5), self.logger, self.call_id, self.method)

            tuple(wrapper)

            self.assertEqual(len(self.log_handler.records), 2)
            self.assertEqual(self.log_handler.records[0].getMessage(), "[2X4B] wash_dishes(0)")
            self.assertEqual(self.log_handler.records[1].getMessage(), "[2X4B] wash_dishes(1)")

    async def test_aiter(self):
        wrapper = LogRequestStreamWrapper(range(5), self.logger, self.call_id, self.method)
        # 'async for' is the only way to trigger __aiter__ in python =< 3.9
        self.assertEqual([i async for i in wrapper], list(range(5)))

    async def test_anext(self):
        wrapper = LogRequestStreamWrapper(range(5), self.logger, self.call_id, self.method)

        self.assertEqual(await anext(wrapper), 0)
        self.assertEqual(await anext(wrapper), 1)

        self.assertEqual(len(self.log_handler.records), 2)
        self.assertEqual(self.log_handler.records[0].getMessage(), "[2X4B] wash_dishes(0)")
        self.assertEqual(self.log_handler.records[1].getMessage(), "[2X4B] wash_dishes(1)")

    async def test_anext_stopped(self):
        wrapper = LogRequestStreamWrapper[int]((), self.logger, self.call_id, self.method)
        with self.assertRaises(StopAsyncIteration):
            await anext(wrapper)

        self.assertEqual(len(self.log_handler.records), 0)

    async def test_aconsume(self):
        # A set of test to consumer the iterator.
        data = (
            # input, output
            ((), ()),
            (range(5), tuple(range(5))),
            (tuple(range(5)), tuple(range(5))),
            (aiter(range(5)), tuple(range(5))),
        )
        for input, output in data:
            with self.subTest(input=input):
                wrapper = LogRequestStreamWrapper(input, self.logger, self.call_id, self.method)
                self.assertEqual(await atuple(wrapper), output)

    async def test_alog_max_count(self):
        with patch("frgal._utils.MESSAGE_MAX_COUNT", 2):
            wrapper = LogRequestStreamWrapper(range(5), self.logger, self.call_id, self.method)

            await atuple(wrapper)

            self.assertEqual(len(self.log_handler.records), 2)
            self.assertEqual(self.log_handler.records[0].getMessage(), "[2X4B] wash_dishes(0)")
            self.assertEqual(self.log_handler.records[1].getMessage(), "[2X4B] wash_dishes(1)")
