"""Utilities for gRPC services."""

import logging
from collections.abc import AsyncIterator, Awaitable, Iterator
from functools import wraps
from typing import Any, Callable, TypeVar, Union, cast

from google.protobuf.message import Message
from grpc import ServicerContext
from grpc.aio import ServicerContext as AsyncServicerContext

from ._utils import get_call_id, lazy_format_message
from .exceptions import ServiceError

RequestType = Union[Message, Iterator[Message]]
UnaryFunc = TypeVar(
    "UnaryFunc",
    Callable[[Any, Message, ServicerContext], Message],
    Callable[[Any, Iterator[Message], ServicerContext], Message],
)
AsyncUnaryFunc = TypeVar(
    "AsyncUnaryFunc",
    Callable[[Any, Message, AsyncServicerContext], Awaitable[Message]],
    Callable[[Any, Iterator[Message], AsyncServicerContext], Awaitable[Message]],
)
StreamFunc = TypeVar(
    "StreamFunc",
    Callable[[Any, Message, ServicerContext], Iterator[Message]],
    Callable[[Any, Iterator[Message], ServicerContext], Iterator[Message]],
)
AsyncStreamFunc = TypeVar(
    "AsyncStreamFunc",
    Callable[[Any, Message, AsyncServicerContext], AsyncIterator[Message]],
    Callable[[Any, Iterator[Message], AsyncServicerContext], AsyncIterator[Message]],
)


def wrap_unary(func: UnaryFunc) -> UnaryFunc:
    """Decorate a gRPC service method which returns a single message."""

    @wraps(func)
    def wrapper(self: Any, request: RequestType, context: ServicerContext) -> Message:
        call_id = get_call_id()
        logger = logging.getLogger(func.__module__)
        logger.debug("[%s] %s(%s)", call_id, func.__qualname__, lazy_format_message(request))
        try:
            reply = func(self, request, context)  # type: ignore[arg-type]
        except ServiceError as error:
            logger.info("[%s] %s failed with %r", call_id, func.__qualname__, error)
            context.abort(error.status, error.message)
        except Exception as error:
            logger.error("[%s] %s raised error %r", call_id, func.__qualname__, error)
            raise
        logger.debug("[%s] %s returned %s", call_id, func.__qualname__, lazy_format_message(reply))
        return reply

    return cast(UnaryFunc, wrapper)


def awrap_unary(func: AsyncUnaryFunc) -> AsyncUnaryFunc:
    """Decorate a gRPC service method which returns a single message."""

    @wraps(func)
    async def wrapper(self: Any, request: RequestType, context: AsyncServicerContext) -> Message:
        call_id = get_call_id()
        logger = logging.getLogger(func.__module__)
        logger.debug("[%s] %s(%s)", call_id, func.__qualname__, lazy_format_message(request))
        try:
            reply = await func(self, request, context)  # type: ignore[arg-type]
        except ServiceError as error:
            logger.info("[%s] %s failed with %r", call_id, func.__qualname__, error)
            await context.abort(error.status, error.message)
        except Exception as error:
            logger.error("[%s] %s raised error %r", call_id, func.__qualname__, error)
            raise
        logger.debug("[%s] %s returned %s", call_id, func.__qualname__, lazy_format_message(reply))
        return reply  # type: ignore[return-value] # https://github.com/python/mypy/issues/14944

    return cast(AsyncUnaryFunc, wrapper)


def wrap_stream(func: StreamFunc) -> StreamFunc:
    """Decorate a gRPC service method which returns a stream."""

    @wraps(func)
    def wrapper(self: Any, request: RequestType, context: ServicerContext) -> Iterator[Message]:
        call_id = get_call_id()
        logger = logging.getLogger(func.__module__)
        logger.debug("[%s] %s(%s)", call_id, func.__qualname__, lazy_format_message(request))
        try:
            reply = func(self, request, context)  # type: ignore[arg-type]
            for item in reply:
                logger.debug("[%s] %s returned %s", call_id, func.__qualname__, lazy_format_message(item))
                yield item
        except ServiceError as error:
            logger.info("[%s] %s failed with %r", call_id, func.__qualname__, error)
            context.abort(error.status, error.message)
        except Exception as error:
            logger.error("[%s] %s raised error %r", call_id, func.__qualname__, error)
            raise

    return cast(StreamFunc, wrapper)


def awrap_stream(func: AsyncStreamFunc) -> AsyncStreamFunc:
    """Decorate a gRPC service method which returns a stream."""

    @wraps(func)
    async def wrapper(self: Any, request: RequestType, context: AsyncServicerContext) -> AsyncIterator[Message]:
        call_id = get_call_id()
        logger = logging.getLogger(func.__module__)
        logger.debug("[%s] %s(%s)", call_id, func.__qualname__, lazy_format_message(request))
        try:
            reply = func(self, request, context)  # type: ignore[arg-type]
            async for item in reply:  # pragma: no branch  # Python 3.9 coverage bug
                logger.debug("[%s] %s returned %s", call_id, func.__qualname__, lazy_format_message(item))
                yield item
        except ServiceError as error:
            logger.info("[%s] %s failed with %r", call_id, func.__qualname__, error)
            await context.abort(error.status, error.message)
        except Exception as error:
            logger.error("[%s] %s raised error %r", call_id, func.__qualname__, error)
            raise

    return cast(AsyncStreamFunc, wrapper)
