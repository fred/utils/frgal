"""Frgal - FRED gRPC auxiliary library."""

from .aio import AsyncGrpcClient
from .client import GrpcClient, make_credentials
from .decoder import GrpcDecoder

__version__ = "4.1.0"
__all__ = ["AsyncGrpcClient", "GrpcClient", "GrpcDecoder", "make_credentials"]
