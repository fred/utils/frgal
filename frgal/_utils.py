"""Internal frgal utilities.

This module is not part of the public API and can change any time
without notice. You're discouraged from using it outside of frgal.
"""

import logging
import random
import string
from collections.abc import AsyncIterable, AsyncIterator, Iterable, Iterator
from typing import Any, Generic, Optional, TypeVar, Union, cast

from asyncstdlib import anext, iter as aiter
from google.protobuf.message import Message
from google.protobuf.text_format import MessageToString

X = TypeVar("X")
AnyIterable = Union[Iterable[X], AsyncIterable[X]]

# Maximal length of a single message representation.
MESSAGE_MAX_LENGTH = 256
# Maximal number of messages from stream that are logged.
MESSAGE_MAX_COUNT = 20


def get_call_id() -> str:
    """Return random call ID for logging."""
    return "".join(random.choice(string.ascii_letters + string.digits) for _ in range(4))  # noqa: S311


def format_message(message: Any) -> str:
    """Format a message to string.

    If argument is a gRPC Message, format it nicely.
    If it's another object, use simple str conversion.
    In any case the output has limited length.
    """
    if isinstance(message, Message):
        out = MessageToString(message, as_one_line=True)
    else:
        out = str(message)
    if len(out) > MESSAGE_MAX_LENGTH:
        return out[: MESSAGE_MAX_LENGTH - 3] + "..."
    else:
        return out


class lazy_format_message:
    """Lazy format a message to string.

    If argument is a gRPC Message, format it nicely.
    If it's another object, use simple str conversion.
    In any case the output has limited length.
    """

    def __init__(self, message: Any):
        self._message = message

    def __repr__(self) -> str:
        return format_message(self._message)


class LogRequestStreamWrapper(Generic[X]):
    """Utility to wrap an iterable and log it when iterated."""

    def __init__(self, requests: AnyIterable[X], logger: logging.Logger, call_id: str, method: str):
        self.requests = requests
        self._iter: Optional[AnyIterable[X]] = None
        self.logger = logger
        self.call_id = call_id
        self.method = method
        self.counter = 0

    def __iter__(self) -> "LogRequestStreamWrapper":
        return self

    def __next__(self) -> X:
        assert isinstance(self.requests, Iterable)  # noqa: S101
        if self._iter is None:
            self._iter = iter(self.requests)
        assert isinstance(self._iter, Iterator)  # noqa: S101
        result = cast(X, next(self._iter))
        self.counter += 1
        if self.counter <= MESSAGE_MAX_COUNT:
            self.logger.debug("[%s] %s(%s)", self.call_id, self.method, lazy_format_message(result))
        return result

    def __aiter__(self) -> "LogRequestStreamWrapper":
        return self

    async def __anext__(self) -> X:
        if self._iter is None:
            self._iter = aiter(self.requests)
        assert isinstance(self._iter, AsyncIterator)  # noqa: S101
        result = cast(X, await anext(self._iter))
        self.counter += 1
        if self.counter <= MESSAGE_MAX_COUNT:
            self.logger.debug("[%s] %s(%s)", self.call_id, self.method, lazy_format_message(result))
        return result
