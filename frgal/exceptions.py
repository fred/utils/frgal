"""Frgal exceptions."""

from grpc import StatusCode


class FrgalException(Exception):
    """Base exception for all frgal exceptions."""


class ServiceNotFound(FrgalException):
    """Exception thrown in case service stub cannot be found in any gRPC module."""


class DecoderError(FrgalException):
    """Error raised in case gRPC message cannot be decoded."""


class UnknownError(FrgalException):
    """Error raised in case gRPC returned unknown error."""


# Service exceptions
class ServiceError(FrgalException):
    """Base exception for error in service."""

    def __init__(self, status: StatusCode, message: str = ""):
        super().__init__(status, message)
        self.status = status
        self.message = message
