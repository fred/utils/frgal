=====
FRGAL
=====

FRED gRPC auxiliary library

This library contains the basic code for gRPC clients for FRED services.


HOWTO
=====

Compile test protobuf
---------------------

.. code-block:: sh

    cd frgal/tests
    python -m grpc_tools.protoc -I . --python_out=. proto/*.proto
